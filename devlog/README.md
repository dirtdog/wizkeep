# Day 0 - Preparation

In preparation for the game jam, worked on creating the git repo and compiling ECL wasm with Docker (actually podman).

# Day 1 - Pain

## Lots of pain

- Spent most day fiddling with the Dockerfiles to get ECL wasm building.
  In the end managed to get Dockerfiles that are able to build ECL wasm, and can then be run and attached to to host the html etc. and accessed from the browser.
  Was unable to interact meaningfully with the running image, other than using the emscripten `ccall` in the Firefox debugger to call some ECL C functions.
- Tried (unsuccessfully) to compile the project as a static C library using ECL and linking it to a simple C application, following the [ECL manual](https://ecl.common-lisp.dev/static/files/manual/current-manual/System-building.html#Compiling-with-ASDF).
  The reason is that I believe this might be necessary to do in an emscripten build.
  Steps more or less:
  - Build ECL from source: The Gentoo ebuild of ECL uses a system package for asdf, but we must use the bundled asdf from ecl/contrib, otherwise we will lack the required `ASDF:MAKE-BUILD` function.
  - Messing with environment variables: Discovered that Guix sets among others `XDG_CONFIG_DIRS` and `XDG_DATA_DIRS` (at least in my setup).
	This makes it so asdf prefers CL "systems" from the Guix store.
	This works fine for SBCL, but for whatever reason fails due to some permission error for ECL.
	Made a script to unset these variables (`unset-guix.lisp`).
  - Installed [Quicklisp](https://www.quicklisp.org/beta/).
  - Followed the instructions in the ECL manual mentioned above, managed to compile after adding necessary flags to `gcc` from `ecl-config --cflags` and `ecl-config --libs`.
  - Got strange error when trying to run the executable, shown below, so gave up on this for now.

```
Condition of type: SIMPLE-ERROR
The packages
  ((ASDF/LISP-ACTION . #<ASDF/LISP-ACTION package>) (ASDF/OPERATE . #<ASDF/OPERATE package>))
were referenced in compiled file
  NIL
but have not been created
Available restarts:

1. (IGNORE) Ignore the error, and try the operation again

Top level in: #<process TOP-LEVEL 0x7f06e4889f00>.
```

## Slightly less pain

Hacked together some SDL2 stuff following examples from [cl-sdl2-tutorial](https://github.com/TatriX/cl-sdl2-tutorial) and some blogs/videos online.
Opened the tileset in Gimp to find pixel positions of sprites.
End result is loading sprites from the Urizen tileset (see *assets/*) in a grid.

![Image of a grid of sprites with the @ character, green snake, red crab and open door sprites from the Urizen tileset.](day1-sprite-grid.png "SDL window with grid of sprites")

# Day 2 - Refactoring and Mapgen

- Added preliminary support for entities (`entity-store`) and levels (`level-store`).
- Updated SDL code to display the current level and move the player (`@` sprite) using vim keys (hjkl).
- Started working on simple BSP map generator.
  Can currently generate some "rooms" but need more logic for padding, removing bad rooms and such, as well as connecting rooms.

The updated SDL window with the player sprite is shown below:
![Image of an SDL window with a black background, dots for each floor tile, and a single @ symbol for the player.](day2-grid-with-player.png "SDL window with player sprite")

Sample output from current BSP mapgen:
```
;; (wizgen::generate-level 48 64) ; =>

################################################################
###.......##..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
############..##..##.....##..#####....##.....##..##..##..##....#
############..##..##.....##..#####....##.....##..##..##..##....#
############..##..##.....##..#####....##.....##..##..##..##....#
############..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
###.......##..##..##.....##..#####....##.....##..##..##..##....#
############..##..##.....##..######################..##..##....#
############..##..##.....##..######################..##..##....#
################..##.....##..##..................##..##..##....#
################..##.....##..##..................##..##..##....#
###...........##..##.....##..######################..##..##....#
###...........##..##.....##..######################..##..##....#
################..##.....##..##...##...##.......###..##..##....#
################..##.....##..##...##...##.......###..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...############..##..##....#
######....##..##..##.....##..##...##...##.......###..##..##....#
######....##..##..##.....##..##...##...##.......###..##..##....#
################..##.....##..##...##...##.......###..##..##....#
################..##.....##..##...##...############..##..##....#
###...........##..##.....##..##...##...############..##..##....#
###...........##..##.....##..##...##...############..##..##....#
###...........##..##.....##..######################..##..##....#
####################.....##..######################..##..##....#
################################################################
################..##############################################
###...........##..##..##########################################
###...........##..##..##########################################
###...........##..##..##......##....########################...#
################..##..##......##....##########............##...#
################..##..##......##....##......##............##...#
#########.....##..##..##########....##......##............##...#
#########.....##..##..##########################################
#########.....##..##..##########################################
#########.....##..##..##########################################
################################################################
```

# Day 3 - More Mapgen

Spent the day refactoring and updating the mapgen code.
Also added support for rendering the generated maps in the SDL window.
Currently the mapgen can generate rooms decently (though some are quite long or quite tiny), as well as generate corridors between the rooms.
The corridor generation is incomplete, as some rooms are not being connected.
An image of a map generated by the current iteration of the mapgen is shown below.

![SDL window with BSP generated rooms from the WIP map generator. The floors of the rooms have the . sprite with black background, the walls have a grey-blue-ish brick wall background, and the corridors have a light brown background.](day3-mapgen.png "SDL mapgen preview.")

# Day 4

## Mapgen: Connecting All Rooms

As mentioned in the day 3 devlog, some rooms were not connected as expected by the mapgen algorithm.
Rather than debugging this I decided to implement Dijkstra's algorithm and fill in the missing corridors using that.
The new approach for adding corridors is the following:
- From the BSP root, recursively add a corridor between the closest leaves of each subtree.
  This was the approach already used.
  It leaves some missing corridors (some rooms are unreachable).
- Select a random point on a floor tile in the current iteration of the map (which includes some corridors).
  Use Dijkstra's algorithm from this point to find a path to any other point in the map.
- From the center of each leaf (room) of the BSP tree, backtrack to the random starting point used above in Dijkstra's algorithm.
  Whenever the path crosses a wall, turn it into a corridor.

In the end this approach at least connects all rooms, although there are usually a bit more corridors than I'd like.

Below is an illustration of the generated maps.
The brown tile corridors are generated by traversing the BSP tree, while rainbow-colored `?` tiles are generated from the output of Dijkstra's algorithm as described above.

![Image of a generated map (tiles) with 8 rectangular rooms surrounded by walls. Some rooms have corridors with a brown tile with others have corridors with a rainbow-colored question mark.](day4-mapgen-dijkstra.png "Generated map with all rooms accessible.")

### Further Implementation Details on Dijkstra's Algorithm

The algorithm was implemented closely following the [pseudocode presented on Wikipedia](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).
Every tile in the map was considered a vertex with edges to neighboring (above, below, left or right).
Wall tiles were given a high weight (1000) to give low priority, while floor tiles were given 1 weight.

The "priority queue" used in the implementation is extremely inefficient.
It's just a list that is sorted before every dequeue.
Ideally something like a [Fibonacci heap](https://en.wikipedia.org/wiki/Fibonacci_heap) should be used for this scenario (highly connected graph, need to reorder entries often).

## Gameplay: Player Can Move in Generated Map

Now that maps are generated, I made the player spawn in an autogenerated levels when the game starts.
I also refactored the SDL code a bit to separate logic more.
Additionally there is some support for collision detection.
In other words the player can move around in the autogenerated level and collides with walls (mostly).

Caveats:
There are some bugs due to refactorings and moving of the code:
- Movement is rotated (e.g. *up* input goes to the left).
- Collision doesn't work in all cases.
- A "shadow" player sprite is rendered.

Below is an illustration of the player rendered on the map (that can move) as well as the "shadow sprite".

![Image of a generated map (tiles) with 8 rectangular rooms surrounded by walls and connected by corridors with brown tiles. In 2 of the rooms the player sprite (@) is rendered.](day4-level-with-player.png "Generated with 2 player sprites.")

## Under the Hood

Started some work under the hood to support game logic.

### Lightweight ECS: Traits

I want to avoid class hierarchies and inheritance for the core game logic, since I want to be able to add/remove properties from entities at runtime (without MOP magic).
Basically I want a lightweight version of ECS.
At the moment performance is not a concern, so I'm not going to go all-in on ECS and components in separate "stores", etc.
I came up with "traits".
Every entity has a traits slot which is either nil or a hash table mapping symbols to some object.
At the moment there are just 2 traits HP and COLLISION (might move collision to be a slot of entity though).

### Planning Ahead

Other than that I've started thinking about "who" is responsible for storing and updating the map state, handling player actions etc.
Currently the *entity store* assigns IDs to entities and stores them in a hash table mapping from ID to entity, while the *level store* stores a hash table of 2D arrays with "environment tiles" (walls, corridors, floors) for each depth.
The SDL renderer checks the environment from the level store each rendering pass, and further renders "visible entities" on top.

It seems like it will be useful for the entity store to be able to (i) access "actors" (entities that may have some AI or other functionality attached) quickly; (ii) check if any entity exists by position.
Probably this information either needs to reside in the entity store or in another "store" that can access both the entity store and level store.
With these pieces in place it will be easier to do things like collision checking and AI.

# Day 5

Mostly backend changes today.
I worked on the following:
- Fixed movement and collision.
- Added support for attacking and implemented bump attacking.
- Optimized implementation of Dijkstra's algorithm.

Below are a couple images of attacking a snake by moving into it ("bump attacking").

Player attacking the snake:
![SDL window with 2 rooms and 2 sprites (player as @ and an enemy as a green snake). REPL output shows the player dealing damage to the snake.](day5-attack-snake.png "Player attacking snake.")

Snake dies after taking a number of attacks:
![SDL window with 2 rooms and 1 sprite (player as @). REPL output shows the player dealing damage to the snake and a message saying the snake dies.](day5-kill-snake.png "Snake dying.")

## Optimizing Dijkstra's Algorithm

As mentioned previously the original implementation used a very inefficient queue (sorted a huge list on every dequeue).
Even with relatively small graphs (some thousands of elements) this implementation would take minutes to complete.
Instead I used `serapeum:heap` as a priority queue, and added tracking for which elements are in the queue as a bit-array.
The new implementation is decently fast, as we will see below.

I also tried a few variants of the implementation.
The implementation uses cons cells to represent vertex positions in the algorithm.
Cons cells are a bit annoying to work with, so I also tried using structs and lists for the same purpose, but discovered it was significantly slower.

For reference the following struct definition was used to for representing a point, and for lists a list like (X Y) was used.

```lisp
(defstruct point2
  (x 0 :type fixnum)
  (y 0 :type fixnum))
```

The following runtimes are from running the algorithm variants (using cons cells, structs or lists to represent vertex positions) in SBCL for a 1024 by 1024 map (>1 million entries):

```
DIJKSTRA WITH CONS:
Evaluation took:
  0.760 seconds of real time
  0.763024 seconds of total run time (0.762476 user, 0.000548 system)
  [ Real times consist of 0.036 seconds GC time, and 0.724 seconds non-GC time. ]
  [ Run times consist of 0.036 seconds GC time, and 0.728 seconds non-GC time. ]
  100.39% CPU
  2,054,543,481 processor cycles
  218,184,640 bytes consed

DIJKSTRA WITH STRUCT:
Evaluation took:
  1.110 seconds of real time
  1.109124 seconds of total run time (1.002523 user, 0.106601 system)
  [ Real times consist of 0.140 seconds GC time, and 0.970 seconds non-GC time. ]
  [ Run times consist of 0.137 seconds GC time, and 0.973 seconds non-GC time. ]
  99.91% CPU
  2,985,322,149 processor cycles
  285,245,360 bytes consed

DIJKSTRA WITH LIST:
Evaluation took:
  0.840 seconds of real time
  0.841811 seconds of total run time (0.821485 user, 0.020326 system)
  [ Real times consist of 0.123 seconds GC time, and 0.717 seconds non-GC time. ]
  [ Run times consist of 0.126 seconds GC time, and 0.716 seconds non-GC time. ]
  100.24% CPU
  2,266,198,695 processor cycles
  285,281,232 bytes consed
```

Surprisingly, the struct-based implementation is around 46% slower, while the list-based implementation is just around 10% slower.
Probably these numbers would change by tuning the compiler.

# Day 6 - Monster AI and Scheduling

Today was spent implementing the foundations for monster AI, scheduling/time, and a functioning game loop.
The scheduling seems to mostly, work, except some rendering glitches.

## Simple AI Algorithm

Since I already implemented Dijkstra's algorithm, I decided to use it for the AI as well, rather than something like A*.
I found a couple of inspiring articles on RogueBasin[1,2] about using Dijkstra maps for monster AI, which motivated me that this was a decent idea.

The AI algorithm is more or less the following:
- Generate Dijkstra map from the player.
- From the map, get the path to the player (the `dijkstra` function returns the weighted distances to each position from the player, as well as "back pointers" for each position, which can be traversed recursively to produced the path to the player).
- The `simple-monster-ai` function takes as parameters the path to the player, a `failp` function and `successp` function.
  For each position in the path, it calls `failp`, and returns its value if present.
  Otherwise, it calls `successp`, and returns the starting position of the path ("next step" of the monster) and the final position (player position).
  This mechanism allows to easily put simple restraints to the monster AI, e.g. `failp` can fail if the distance is above 5 steps away.

## Scheduling and Time

I needed a simple scheduling functionality.
I also know that I want to enable actions/actors with different speeds.
Looking at RogueBasin I found some articles, and decided to follow one with a simple priority-based scheduling scheme[3].

Basic scheduling functionality using `serapeum:heap` as a priority queue was added.
Actors (including the player) are scheduled for some time ahead (using a `'SPEED` trait to determine how far into the future they are scheduled).
When dequeued, the game logic will determine if the actor is a player or enemy.
For the player, it will use the last given input as its action (e.g. move).
For the enemy, it will run the aforementioned AI function to determine the next action (currently just moving towards the player, and attack if at the neighboring tile).

## Game Loop Updates

Up until this point, there wasn't really any game loop, just accepting player input and acting on it directly.
To fix this, I added optional parameters to the SDL function `ui-toplevel` called `handle-inputs` and `run-turns`.
Both are callback functions.
- `handle-inputs` is called for input events other than `:quit`.
  It currently just puts the player input on a stack.
  When the player is dequeued from the scheduler, the most recent input is taken as its action, while the others are discarded.
- `run-turns` is called in the `:idle` portion of the SDL event loop.
  Basically it runs one iteration of the game loop that was briefly explained above.
  All actors are dequeued from the scheduler and processed, until the next candidate for scheduling is the player without any more inputs.
  Rendering is performed after running the callback function.

As mentioned, there are some rendering glitches.
The enemy seems to be rendered at the incorrect position.
It still dies eventually to the player's bump attacks, at which point the dead enemy is no longer rendered.

## References

1. [Dijkstra Maps Visualized](https://roguebasin.com/index.php/Dijkstra_Maps_Visualized)
2. [The Incredible Power of Dijkstra Maps](https://roguebasin.com/index.php/The_Incredible_Power_of_Dijkstra_Maps)
3. [A priority queue based turn scheduling system](https://roguebasin.com/index.php/A_priority_queue_based_turn_scheduling_system)

# Day 7 - Refactoring and Tweaking

Didn't do much today, other than fixing some bugs, refactoring the code a bit, and tweaking the map generator.
- Mapgen: Added some constraints to ensure maps are not containing a single huge room or very little space (minimum rooms, minimum floor area vs map size ratio).
- Fixed bug where enemies moved to incorrect positions.
- Some functions were getting huge, so refactored them into smaller functions.
- Spawn monster at random position in the start of the game.

When spawning the monster at a random position, it became obvious that it's getting too many turns.
Probably need to fix the scheduling code.

# Day 8 - Bugfixes, Generating Monsters, Lots of Refactoring

## Level Generation

Most important updates today were adding support for generating monsters.
To save time, I tried to reuse as much of the existing code as possible.
The simplest approach I could think of for monster generation was reusing the data structures used by the BSP map generator during map generation.

The map generator generates a "BSP tree", represented as a tree of nodes.
Each nodes has 4 fields: parent, left child, right child, value.
Only the root node has parent NIL, and only the leaf nodes have no children and meaningful values.
In particular, the value of each leaf node is a rectangle represented as some starting point *(x1 y1)* and end point *(x2 y2)*.

Since it's not necessary to spawn monsters in corridors, we can reuse these values.
I added an additional slot `room-entity-mapper` to the `bsp-mapgen` class for this purpose.
It's a function that takes a room and returns an alist mapping positions to *something* (in practice, a character or symbol).
Then, we just get the values of the leaves of the BSP tree (room rectangles), apply `room-entity-mapper` to each room, and update the map with the return values.
The caller can just set another function in the `room-entity-mapper` slot for alternative monster generation, for example to reduce monster density or add additional monster types on deeper levels.

![SDL window showing generated level with around 20 rooms, most of them filled with randomly placed green snakes, and in the lower right room the player @ symbol is surrounded by 4 snakes.](day8-level-with-monsters.png "Level with monsters generated by updated generator")

An example level is displayed in the image above.
In practice I used a distribution function `(random (ceiling (log (area rect))))` for each room (`rect`).
As you can see, it is a random number from 0 to logarithm of the room size, which ensures that some rooms are empty, and that huge rooms don't have way too many monsters.
The monsters are placed randomly in each room

## Monster AI in Action

After fixing the remaining scheduling bugs, it was possible to start working on the monster AI.
I decided to keep using Dijkstra's algorithm for monster pathing as well.
Ideally I should be using something like A*, but I don't want to spend time implementing it this late in the jam.
For optimization, I added a parameter to `dijkstra` that allows you to stops the algorithm when you find the path to a destination.

For each turn, the monster finds the shortest path to the player using `dijkstra` and walks towards the player.
When next to the player, it walks into the player, performing a bump attack.
Currently there is only the snake monster, which moves slowly and only in "cardinal" directions (left, right, up, down).

While it looked a bit funny, we don't want all the monsters on the entire level to chase us (also for efficiency's sake).
Lacking a field of view algorithm, I just used the length of the 2-dimensional vector between the monster position and the player position (*sqrt(x_difference^2 + y_difference^2)*).
If this value exceeds the arbitrary number 20, the monster passes its turn.

A funny bug to mention was that the player wasn't getting scheduled as expected, so monsters would take many turns in a row.
Further the monsters could see the player across the whole map before limiting their range.
So every monster in the dungeon would chase the player and kill it in seconds.

## The Boring Stuff

Other than that was mostly refactoring and bugfixing as some of the code was becoming quite hard to manage.
Also added macro for generating weapons, and added different output strings for different weapons (for example, the snake bites you rather than hit you).

# Day 9 - Props and New Mechanic

## Props

Added "props".
They are things on the dungeon floor that are not actors.
Most common examples are items that can be picked up (e.g. weapons or keys).
Other examples are chests.

Implemented logic for following:
- Monsters drop items in their inventory as props when they die.
- When the player walks over a "collectible" prop, it automatically gets put in its inventory.
- `CONTAINER-PROP` class which has common logic for containers (mostly envisioned as a chest).
  - The container may block player movement if it is locked.
	Walking into a blocked container doesn't count as a turn.
  - An arbitrary check (e.g. *actor A has key K in its inventory*) is used to check if the container can be opened.
  - When the check passes, the container state changes to open, and its contents are displayed.
  - Walking over the container causes the player to pick up its contents.

### Illustration

Fresh map with a chest and key spawned next to the player (also monsters).:

![Fresh map with a chest and key spawned next to the player (also monsters).](day9-closed-chest.png "Map with key, closed chest and enemies.")

Player walks over the key to pick it up.
Player also killed a snake, which dropped an item with sprite `)`:

![Map from above, but without the key and with 1 of the snakes dead, which dropped an item with sprite right parenthesis.](day9-pickup-key-and-monster-drop.png "Map from above, with a monster drop and the key picked up.")

Player walks into the chest with the key in its inventory, and the chest opens, revealing some loot inside (a book):

![Map from above, but with a book sprite where the chest used to be.](day9-chest-open.png "Map from above, with chest open, and monster drop picked up.")

If the player walks into onto the chest prop the book will be picked up and the sprite will show an empty open chest instead.

## GC Mechanic

Our protagonist likes things to be neat and tidy, even in harsh times.
When the player performs actions such as attacking (not movement) its GC counter increases.
When the GC counter exceeds its maximum, the player spends some turns idle "collecting garbage".
Unfortunately he hasn't heard of parallel GC techniques.

The player is idle for some turns (passes turns), while monsters can keep acting.
After tidying up, the player feels refreshed, and its HP is restored.

This mechanic is kind of like a forced rest mechanic, and can foreseeably be used for some tactical play.
Also, this makes a hunger bar and health potions unnecessary.

## Boring

Also spend way too long fixing bugs in scheduler times.

# Day 10 - Whole Lotta Stuff

Added
- Procedural generation of chests and staircases
- Ability to go further into the dungeon (generating new maps) by interacting with staircases
- Loot tables for enemies: E.g. a monster may drop a key for a chest
- New enemies: Coffee Recipe, Coffee Factory, Oxidated Crab, Scheming Wizard
- Rudimentary FoV
- Status bar for player stats and inventory
- Text log for events
- Build instructions
