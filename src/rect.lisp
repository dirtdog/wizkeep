;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defstruct (rect (:constructor rect (x1 x2 y1 y2)))
  (x1 0 :type fixnum)
  (x2 0 :type fixnum)
  (y1 0 :type fixnum)
  (y2 0 :type fixnum))

(defgeneric area (room)
  (:method ((rect rect))
    (* (1+ (- (rect-x2 rect) (rect-x1 rect)))
       (1+ (- (rect-y2 rect) (rect-y1 rect))))))

(defun rect-from-pairs (a b)
  (rect (min (car a) (car b))
        (max (car a) (car b))
        (min (cdr a) (cdr b))
        (max (cdr a) (cdr b))))

(defgeneric rect-dimensions (rect &key include-upper-bound)
  (:documentation "Returns cons cell (HEIGHT . WIDTH) of RECT.")
  (:method ((rect list) &key include-upper-bound)
    (destructuring-bind (x1 x2 y1 y2)
        rect
      (cons (+ (- x2 x1) (if include-upper-bound 1 0))
            (+ (- y2 y1) (if include-upper-bound 1 0)))))
  (:method ((rect rect) &key include-upper-bound)
    (cons (+ (- (rect-x2 rect) (rect-x1 rect)) (if include-upper-bound 1 0))
          (+ (- (rect-y2 rect) (rect-y1 rect)) (if include-upper-bound 1 0)))))

(defgeneric len (space)
  (:method ((rect rect))
    (destructuring-bind (height . width)
        (rect-dimensions rect)
      (sqrt (+ (expt height 2) (expt width 2))))))

(defgeneric vector-length-8 (rect)
  (:documentation "Chebyshev distance.")
  (:method ((rect rect))
    (with-slots (x1 x2 y1 y2) rect
      (max (- x2 x1) (- y2 y1)))))

(defgeneric rect-dimensions< (rect height width &key include-upper-bound)
  (:method ((rect list) height width &key include-upper-bound)
    (destructuring-bind (h . w)
        (rect-dimensions rect :include-upper-bound include-upper-bound)
      (or (< h height)
          (< w width))))
  (:method ((rect rect) height width &key include-upper-bound)
    (destructuring-bind (h . w)
        (rect-dimensions rect :include-upper-bound include-upper-bound)
      (or (< h height)
          (< w width)))))

(defgeneric rect< (recta rectb)
  (:method ((recta list) (rectb list))
    (loop for x in recta
          for y in rectb
          do (when (< x y)
               (return-from rect< t)))
    nil)
  (:method ((a rect) (b rect))
    (or (< (rect-x1 a) (rect-x1 b))
        (< (rect-x2 a) (rect-x2 b))
        (< (rect-y1 a) (rect-y1 b))
        (< (rect-y2 a) (rect-y2 b)))))

(defgeneric rect-contains (recta rectb)
  (:method ((recta list) (rectb list))
    (and (<= (first recta) (first rectb))
         (>= (second recta) (second rectb))
         (<= (third recta) (third rectb))
         (>= (fourth recta) (fourth rectb))))
  (:method ((a rect) (b rect))
    (and (<= (rect-x1 a) (rect-x1 b))
         (>= (rect-x2 a) (rect-x2 b))
         (<= (rect-y1 a) (rect-y1 b))
         (>= (rect-y2 a) (rect-y2 b)))))

(defgeneric rect-pad (rect pad-rect &key isvalid)
  (:method ((rect list) (pad-rect list) &key (isvalid #'rect-contains))
    (destructuring-bind (x1 x2 y1 y2)
        rect
      (destructuring-bind (xleft xright yleft yright)
          pad-rect
        (let ((result (rect (+ x1 xleft) (- x2 xright)
                            (+ y1 yleft) (- y2 yright))))
          (values result (funcall isvalid rect result))))))
  (:method ((rect rect) (pad-rect rect) &key (isvalid #'rect-contains))
    (let ((result (rect (+ (rect-x1 rect) (rect-x1 pad-rect))
                        (- (rect-x2 rect) (rect-x2 pad-rect))
                        (+ (rect-y1 rect) (rect-y1 pad-rect))
                        (- (rect-y2 rect) (rect-y2 pad-rect)))))
      (values result (funcall isvalid rect result)))))

(defgeneric center-of (rect)
  (:method ((rect rect))
    (cons (floor (/ (+ (rect-x1 rect) (rect-x2 rect)) 2))
          (floor (/ (+ (rect-y1 rect) (rect-y2 rect)) 2)))))
