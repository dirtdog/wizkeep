;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass entity ()
  ((id :accessor id
       :initarg :id
       :initform 'unset)
   (posx :accessor posx
         :initarg :posx
         :initform 0)
   (posy :accessor posy
         :initarg :posy
         :initform 0)
   (posz :accessor posz
         :initarg :posz
         :initform 0)
   (fov :accessor fov
        :initarg :fov
        :initform 10
        :documentation "Range at which monsters can spot the player.")
   (collision :accessor collision
              :initarg :collision
              :initform nil)
   (traits :accessor traits
           :initarg :traits
           :initform nil
           :documentation "Hash table with traits. Key is a symbol.")
   (look :accessor look
         :initarg :look
         :initform 'sprite-undefined)))

(defmethod print-object ((entity entity) stream)
  (print-unreadable-object (entity stream :type t)
    (with-slots (id posx posy posz look traits) entity
      (format stream "{id ~a; position (~a ~a ~a); look ~a; traits ~a}"
              id posx posy posz look (alexandria:hash-table-keys traits)))))

(defmethod initialize-instance :after ((entity entity) &key traits-plist)
  (when traits-plist
    (if (null (traits entity))
        (setf (traits entity)
              (alexandria:plist-hash-table traits-plist))
        (loop for (k v) on traits-plist by #'cddr do
          (setf (gethash k (traits entity)) v)))))

(defmethod actorp ((entity entity))
  (and (traits entity)
       ;; all actors have HP, and only actors have HP
       (nth-value 1 (gethash 'hp (traits entity)))))

(defmethod blockingp ((entity entity))
  (and (not (actorp entity))
       (collision entity)))

(defmethod has-trait ((entity entity) trait)
  (if (traits entity)
      (gethash trait (traits entity))
      (values nil nil)))

(defmethod trait ((entity entity) key &optional default)
  (gethash key (traits entity) default))

(defmethod name ((entity entity) &optional default)
  (let ((name (trait entity 'name default)))
    (if name
        name
        (symbol-name (look entity)))))

(defmethod take-attack ((entity entity) dmg)
  (let ((new-hp (- (trait entity 'hp) dmg)))
    (setf (gethash 'hp (traits entity)) new-hp)
    (<= new-hp 0)))

(defun set-trait (entity trait value)
  (setf (gethash trait (traits entity)) value))

(defun push-trait-value (entity trait value)
  (push value (gethash trait (traits entity))))

(defmethod pass-turn ((entity entity))
  (setf (gethash 'delay (traits entity))
        (gethash 'speed (traits entity) 1)))

(defmethod rect-between ((entity-a entity) (entity-b entity))
  (rect-from-pairs (cons (posx entity-a) (posy entity-a))
                   (cons (posx entity-b) (posy entity-b))))

(defmethod get-position ((entity entity))
  (list (posx entity) (posy entity) (posz entity)))

(defmethod set-position ((entity entity) (position list))
  (cond
    ((null position)
     (error "position list ~a must have 1 to 3 elements" position))
    ((not (listp (cdr position))) ; => position is cons pair
     (setf (posx entity) (car position)
           (posy entity) (cdr position)))
    (t
     (case (length position)
       (3
        (destructuring-bind (x y z) position
          (setf (posx entity) x
                (posy entity) y
                (posz entity) z)))
       (2
        (setf (posx entity) (first position)
              (posy entity) (second position)))
       (1
        (setf (posx entity) (car position)))
       (otherwise
        (error "position list ~a must have 1 to 3 elements" position)))))
  position)
