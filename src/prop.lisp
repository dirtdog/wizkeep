;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass prop (entity)
  ((collectible :accessor collectible
                :initarg :collectible
                :initform nil)
   (description :accessor description
                :initarg :description
                :initform "")))

(defun %format-description-string (prop)
  (if (not (zerop (length (description prop))))
      (format nil "; desc '~a'" (description prop))
      ""))

(defmethod print-object ((prop prop) stream)
  (print-unreadable-object (prop stream :type t)
    (with-slots (id posx posy posz look collectible) prop
      (format stream "{id ~a; position ~a; look ~a; collectible? ~a~a}"
              id (get-position prop) look collectible (%format-description-string prop)))))

(defun %prop-predicate-noop (entity prop)
  (declare (ignore entity prop))
  nil)

(defun %prop-update-look-noop (prop)
  (declare (ignore prop))
  nil)

(defclass container-prop (prop)
  ((closed :accessor closed
           :initarg :closed
           :initform t)
   (collectible :initform t)
   (content :accessor content
            :initarg :content
            :initform nil
            :documentation "List, probably containing other props.")
   (predicate :accessor predicate
              :initarg :predicate
              :initform #'%prop-predicate-noop)
   (update-look :accessor update-look
                :initarg :update-look
                :initform #'%prop-update-look-noop)))

(defun %prop-chest-update-look (prop)
  (cond
    ((closed prop) 'sprite-chest-closed)
    ((not (null (content prop)))
     (look (car (content prop))))
    (t 'sprite-chest-open)))

(defun %prop-chest-predicate (entity prop)
  (declare (ignore prop))
  (let ((keys (remove-if
               (lambda (prop)
                 (not (and (eq (look prop) 'sprite-key-silver)
                           (= (posz prop) (posz entity)))))
               (trait entity 'inventory))))
    (not (null keys))))

(defun make-chest (row col depth content)
  (make-instance
   'container-prop
   :posx row :posy col :posz depth
   :look 'sprite-chest-closed
   :content content
   :predicate #'%prop-chest-predicate
   :update-look #'%prop-chest-update-look))

(defun make-staircase (row col depth)
  (make-instance 'prop :posx row :posy col :posz depth
                       :collectible nil
                       :look 'sprite-stair-up))

(defmethod print-object ((prop container-prop) stream)
  (print-unreadable-object (prop stream :type t)
    (with-slots (id posx posy posz look collectible closed content) prop
      (format stream "{id ~a; position ~a; look ~a; collectible? ~a; closed? ~a; items? ~a~a}"
              id (get-position prop) look collectible closed (length content) (%format-description-string prop)))))

(defmethod maybe-update-look ((prop prop))
  (let ((updated (funcall (update-look prop) prop)))
    (when updated
      (setf (look prop) updated))))

(defmethod toggle-state ((prop container-prop))
  (let ((new-state (not (closed prop))))
    (setf (closed prop) new-state
          ;; closed => true, open => false
          (collision prop) (not new-state))
    (maybe-update-look prop)
    (if new-state
        'opened
        'closed)))

(defgeneric prop-p (entity)
  (:method (entity)
    nil)
  (:method ((prop prop))
    t))

(defgeneric collect-prop (entity prop)
  (:documentation
   "ENTITY collects contents of PROP. Result is the collected contents, or nil
 if contents were already collected.")
  (:method (entity (prop prop))
    (push-trait-value entity 'inventory prop)
    prop)
  (:method (entity (prop container-prop))
    (let ((content (content prop)))
      (if (and content (trait entity 'collector))
          (progn
            ;; add content to entity's inventory
            (set-trait entity 'inventory
                       (append (trait entity 'inventory) content))
            ;; ensure contents cannot be picked up more than once
            (setf (content prop) nil)
            ;; return collected contents to caller
            content)
          nil))))

(defgeneric move-onto (entity prop)
  (:documentation
   "ENTITY moves onto PROP. Result is (values OUTCOME ENTITY-MOVED). OUTCOME is
 non-nil if action was completed, and ENTITY-MOVED is T if ENTITY position was
 updated as a side effect.")
  (:method (entity (prop prop))
    (if (collision prop)
        (values nil nil)
        (progn
          (set-position entity (get-position prop))
          (if (collectible prop)
              (values (collect-prop entity prop) t)
              (values t t)))))
  (:method (entity (prop container-prop))
    ;; chest scenarios:
    ;; - closed: collision
    ;;   - predicate t? open + display contents + + consume turn
    ;;   - predicate f? 'do-nothing
    ;; - open:
    ;;    - move onto -> pick up items + consume turn
    (if (closed prop)
        ;; closed: try to open container
        (if (funcall (predicate prop) entity prop)
            (values (toggle-state prop) nil)
            (values nil nil))
        ;; TODO might want to separate between who can collect and not, e.g. player or not
        ;; open: add items from prop to inventory
        (progn
          (set-position entity (get-position prop))
          (let ((outcome (collect-prop entity prop)))
            (maybe-update-look prop)
            (values outcome t))))))
