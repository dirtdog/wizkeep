;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass mapgen ()
  ((rows :accessor rows
         :initarg :rows
         :initform 32)
   (cols :accessor cols
         :initarg :cols
         :initform 48)))

(defun %bsp-noop (&rest rest)
  (declare (ignore rest))
  nil)

(defclass bsp-mapgen (mapgen)
  ((min-height :accessor min-height
               :initarg :min-height
               :initform 3)
   (min-width :accessor min-width
              :initarg :min-width
              :initform 4)
   (min-rooms :accessor min-rooms
              :initarg :min-rooms
              :initform 6)
   (min-area% :accessor min-area%
              :initarg min-area%
              :initform .3)
   (room-entity-mapper :accessor room-entity-mapper
                       :initarg :room-entity-mapper
                       :initform #'%bsp-noop
                       :type function
                       :documentation
                       "Function with input room that returns an alist with keys
 cons (row . col) and value something that represent an entity.")
   (rooms-postprocess :accessor rooms-postprocess
                      :initarg :rooms-postprocess
                      :initform #'%bsp-noop))
  (:documentation "Mapgen is rerun until constraints specified by MIN-ROOMS and
 MIN-AREA% are true."))

(defclass btree-node ()
  ((value :accessor value
          :initarg :value
          :initform nil)
   (lchild :accessor lchild
           :initarg :lchild
           :initform nil)
   (rchild :accessor rchild
           :initarg :rchild
           :initform nil)
   (parent :accessor parent
           :initarg :parent
           :initform nil)))

(defgeneric leafp (node)
  (:documentation "True if node has no child nodes.")
  (:method ((node btree-node))
    (and (null (lchild node)) (null (rchild node)))))

(defgeneric rootp (node)
  (:documentation "True of node is the root node.")
  (:method ((node btree-node))
    (null (parent node))))

(defgeneric intermediatep (node)
  (:documentation "True if node is not the root node and has child node(s).")
  (:method ((node btree-node))
    (and (parent node)
         (or (lchild node) (rchild node)))))

;; pseudocode (https://roguebasin.com/index.php/Basic_BSP_Dungeon_generation):
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate leaf sub-dungeons:
;; - start: rectangular dungeon with wall cells
;; - choose random horizontal or vertical split
;; - choose random pos (x row, y col)
;; - split dungeon into 2 sub-dungeons
;; - apply same operation to each sub-dungeon; split position must not be too close to border so that we are able to place a room inside each generated sub-dungeon
;; - repeat until lowest sub-dungeons (leaves) have approx the room-size we want to generate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; build dungeon:
;; - create room of random size in each leaf
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; corridors:
;; - connect each leaf to it's neighbor leaf
;; - up level and repeat process for parent sub-regions
;; - repeat until first two sub-dungeons are connected

(defun random-in-range (from to &key include-upper-bound)
  (+ from (random (+ (- to from) (if include-upper-bound 1 0)))))

(defgeneric random-cutoff-point (rect &key min-height min-vpad min-hpad include-upper-bound)
  (:method ((rect list) &key (min-height 2) (min-width 2) min-vpad min-hpad include-upper-bound)
    (declare (ignorable min-height min-width min-vpad min-hpad))
    (destructuring-bind (x1 x2 y1 y2)
        rect
      (cons (random-in-range x1 x2 :include-upper-bound include-upper-bound)
            (random-in-range y1 y2 :include-upper-bound include-upper-bound))
      ;; TODO consider if some of following logic should be kept (to prevent very small cutoffs)
      ;;
      ;; (destructuring-bind (height . width)
      ;;     ;; TODO instead of minimum padding, calculate padding upto some value
      ;;     (rect-dimensions rect :include-upper-bound include-upper-bound)
      ;;   (let ((min-hpad (if min-hpad min-hpad (floor (/ min-height 2))))
      ;;         (min-vpad (if min-vpad min-vpad (floor (/ min-height 2)))))
      ;;     (if (or (and (not (null min-height))
      ;;                  (< height (+ min-height min-vpad)))
      ;;             (and (not (null min-width))
      ;;                  (< width (+ min-width min-hpad))))
      ;;         nil
      ;;         ;; TODO must use "padding" around the random number
      ;;         ;; (cons (+ (first rect) (random height)) (+ (third rect) (random width)))
      ;;         (cons (random-in-range (+ x1 min-vpad) (- x2 min-vpad)
      ;;                                :include-upper-bound include-upper-bound)
      ;;               (random-in-range (+ y1 min-hpad) (- y2 min-hpad)
      ;;                                :include-upper-bound include-upper-bound)))))
      ))
  (:method ((rect rect) &key (min-height 2) (min-width 2) min-vpad min-hpad include-upper-bound)
    (declare (ignorable min-height min-width min-vpad min-hpad))
    (cons (random-in-range (rect-x1 rect) (rect-x2 rect) :include-upper-bound include-upper-bound)
          (random-in-range (rect-y1 rect) (rect-y2 rect) :include-upper-bound include-upper-bound))))

(defmethod print-object ((node btree-node) stream)
  (print-unreadable-object (node stream :type t)
    (with-accessors ((value value) (lchild lchild) (rchild rchild) (parent parent))
        node
      (let ((prefix
              (cond
                ((null parent) "ROOT")
                ((leafp node) "LEAF")
                (t "NODE")))
            (value-str
              (if value
                  (format nil ", value: ~a" value)
                  ""))
            (suffix
              (if (leafp node)
                  ""
                  (format nil ", (~a . ~a)" lchild rchild))))
        (format stream "~a~a~a" prefix value-str suffix)))))

(defun room-from-leaf (leaf)
  ;; TODO
  ;; (destructuring-bind (x1 x2 y1 y2)
  ;;     (value leaf)
  ;;   ;; random room from minimum size up to rect bounds
  ;;   ;; 1. determine new height and width
  ;;   ;; 2. determine start paddings
  ;;   (let* ((new-xlim (random-in-range x1 x2 :include-upper-bound t))
  ;;          (new-ylim (random-in-range y1 y2 :include-upper-bound t))
  ;;          (pad-x (random (1+ (- x2 new-xlim))))
  ;;          (pad-y (random (1+ (- y2 new-ylim)))))
  ;;     (rect (+ pad-x x1)
  ;;           (+ pad-x new-xlim)
  ;;           (+ pad-y y1)
  ;;           (+ pad-y new-ylim))))
  (value leaf))

(defgeneric generate-child-rects (rect cutoff-cons direction)
  ;; EXAMPLE
  ;; rect = (0 3 0 5)
  ;; =>
  ;;   012345
  ;; 0 ******
  ;; 1 ***@**
  ;; 2 ******
  ;; 3 ******
  ;; => cut = (1 . 3)
  ;;
  ;; horizontal
  ;;   012345
  ;; 0 ******
  ;; 1 ******
  ;;   ---@--
  ;; 2 ******
  ;; 3 ******
  ;; => lchild = (0 1 0 5)
  ;;    rchild = (2 3 0 5)
  ;;
  ;; vertical
  ;;   0123 45
  ;; 0 ****|**
  ;; 1 ****@**
  ;; 2 ****|**
  ;; 3 ****|**
  ;; => lchild = (0 3 0 3)
  ;;    rchild = (0 3 4 5)
  (:method ((rect list) cutoff-cons direction)
    (destructuring-bind (x1 x2 y1 y2)
        rect
      (destructuring-bind (cut-x . cut-y)
          cutoff-cons
        (case direction
          (horizontal
           (values (rect x1 cut-x y1 y2)
                   (rect (1+ cut-x) x2 y1 y2)))
          (vertical
           (values (rect x1 x2 y1 cut-y)
                   (rect x1 x2 (1+ cut-y) y2)))))))
  (:method ((rect rect) cutoff-cons direction)
    (with-slots (x1 x2 y1 y2)
        rect
      (destructuring-bind (cut-x . cut-y)
          cutoff-cons
        (case direction
          (horizontal
           (values (rect  x1 cut-x y1 y2)
                   (rect  (1+ cut-x) x2 y1 y2)))
          (vertical
           (values (rect  x1 x2 y1 cut-y)
                   (rect  x1 x2 (1+ cut-y) y2))))))))

(defgeneric corridor-direction (recta rectb)
  (:method ((recta list) (rectb list))
    (destructuring-bind (ax1 ax2 ay1 ay2)
        recta
      (destructuring-bind (bx1 bx2 by1 by2)
          rectb
        (cond
          ((and (< ay1 by1) (< ay2 by2))
           'horizontal)
          ((and (< by1 ay1) (< by2 ay2))
           'horizontal)
          ((and (< ax1 bx1) (< ax2 bx2))
           'vertical)
          ((and (< bx1 ax1) (< bx2 ax2))
           'vertical)))))
  (:method ((a rect) (b rect))
    (with-slots ((ax1 x1) (ax2 x2) (ay1 y1) (ay2 y2))
        a
      (with-slots ((bx1 x1) (bx2 x2) (by1 y1) (by2 y2))
          b
        (cond
          ((and (< ay1 by1) (< ay2 by2))
           'horizontal)
          ((and (< by1 ay1) (< by2 ay2))
           'horizontal)
          ((and (< ax1 bx1) (< ax2 bx2))
           'vertical)
          ((and (< bx1 ax1) (< bx2 ax2))
           'vertical))))))
;; (corridor-direction (rect 1 6 1 28) (rect 1 6 33 41)) => HORIZONTAL

(defmethod collect-leaves ((node btree-node))
  (labels ((collect-leaves-inner (node)
             (cond
               ((leafp node)
                node)
               ((null (lchild node))
                (collect-leaves-inner (rchild node)))
               ((null (rchild node))
                (collect-leaves-inner (lchild node)))
               (t (cons (collect-leaves-inner (lchild node))
                        (collect-leaves-inner (rchild node)))))))
    (alexandria:flatten (collect-leaves-inner node))))


(defgeneric closest-leaf (node rect)
  (:method ((node btree-node) (rect list))
    (destructuring-bind (x1 x2 y1 y2)
        rect
      (labels ((find-closest (list min min-val)
                 (if (null list)
                     min
                     (let* ((lrect (value (car list)))
                            (diff (+ (abs (- x1 (first lrect)))
                                     (abs (- x2 (second lrect)))
                                     (abs (- y1 (third lrect)))
                                     (abs (- y2 (fourth lrect))))))
                       (if (< diff min-val)
                           (find-closest (cdr list) (car list) diff)
                           (find-closest (cdr list) min min-val))))))
        (let ((leaves (collect-leaves node)))
          (find-closest leaves (car leaves) most-positive-fixnum)))))
  (:method ((node btree-node) (rect rect))
    (labels ((find-closest (list min min-val)
               (if (null list)
                   min
                   (let* ((lrect (value (car list)))
                          (diff (+ (abs (- (rect-x1 rect) (rect-x1 lrect)))
                                   (abs (- (rect-x2 rect) (rect-x2 lrect)))
                                   (abs (- (rect-y1 rect) (rect-y1 lrect)))
                                   (abs (- (rect-y2 rect) (rect-y2 lrect))))))
                     (if (< diff min-val)
                         (find-closest (cdr list) (car list) diff)
                         (find-closest (cdr list) min min-val))))))
      (let ((leaves (collect-leaves node)))
        (find-closest leaves (car leaves) most-positive-fixnum)))))

(defgeneric generate-corridor (rooma roomb)
  (:method ((a rect) (b rect))
    ;; Input: 2 rects
    ;; (rect 1 6 1 28) ; left
    ;; (rect 1 6 33 41) ; right
    ;; Output: (rect 3 3 29 32)
    ;; Illustration: room rects denoted by '.', corridor rect denoted by '='
    ;;                               28    33     41     47
    ;;   0123456789 ...              |     |      |  ... |
    ;; 0 ################################################
    ;; 1 #............................#####........###### ; 1
    ;; 2 #............................#####........######
    ;; 3 #............................=====........###### ; 3
    ;; 4 #............................#####........######
    ;; 5 #............................#####........###### ; 5
    ;; 6 #............................################### ; 6
    ;; 7 ################################################
    (with-slots ((ax1 x1) (ax2 x2) (ay1 y1) (ay2 y2))
        a
      (with-slots ((bx1 x1) (bx2 x2) (by1 y1) (by2 y2))
          b
        (flet ((mean (lower upper)
                 (floor (/ (+ lower upper) 2))))
          (let ((direction (corridor-direction a b)))
            (case direction
              (horizontal
               (let* ((acenterx (mean ax1 ax2))
                      (bcenterx (mean bx1 bx2))
                      (centerx (mean acenterx bcenterx)))
                 (rect centerx centerx (1+ (min ay2 by2)) (1- (max ay1 by1)))))
              (vertical
               (let* ((acentery (mean ay1 ay2))
                      (bcentery (mean by1 by2))
                      (centery (mean acentery bcentery)))
                 (rect (1+ (min ax2 bx2)) (1- (max ax1 bx1)) centery centery)))))))))
  (:method ((recta list) (rectb list))
    ;; Input: 2 rects
    ;; (rect 1 6 1 28) ; left
    ;; (rect 1 6 33 41) ; right
    ;; Output: (rect 3 3 29 32)
    ;; Illustration: room rects denoted by '.', corridor rect denoted by '='
    ;;                               28    33     41     47
    ;;   0123456789 ...              |     |      |  ... |
    ;; 0 ################################################
    ;; 1 #............................#####........###### ; 1
    ;; 2 #............................#####........######
    ;; 3 #............................=====........###### ; 3
    ;; 4 #............................#####........######
    ;; 5 #............................#####........###### ; 5
    ;; 6 #............................################### ; 6
    ;; 7 ################################################
    (unless (or (null recta) (null rectb))
      (destructuring-bind (ax1 ax2 ay1 ay2)
          recta
        (destructuring-bind (bx1 bx2 by1 by2)
            rectb
          (flet ((mean (lower upper)
                   (floor (/ (+ lower upper) 2))))
            (let ((direction (corridor-direction recta rectb)))
              (case direction
                (horizontal
                 (let* ((acenterx (mean ax1 ax2))
                        (bcenterx (mean bx1 bx2))
                        (centerx (mean acenterx bcenterx)))
                   (rect centerx centerx (1+ (min ay2 by2)) (1- (max ay1 by1)))))
                (vertical
                 (let* ((acentery (mean ay1 ay2))
                        (bcentery (mean by1 by2))
                        (centery (mean acentery bcentery)))
                   (rect (1+ (min ax2 bx2)) (1- (max ax1 bx1)) centery centery))))))))))
  (:method ((nodea btree-node) (nodeb btree-node))
    (let ((leafa (leafp nodea))
          (leafb (leafp nodeb)))
      (cond ((and leafa leafb)
             (generate-corridor (value nodea) (value nodeb)))
            (leafb
             (cons (generate-corridor (value (closest-leaf nodea (value nodeb)))
                                      (value nodeb))
                   (generate-corridors nodea)))
            (leafa
             (cons (generate-corridor (value (closest-leaf nodeb (value nodea)))
                                      (value nodea))
                   (generate-corridors nodeb)))
            (t ; both nodea and nodeb are intermediate nodes
             (list (generate-corridor (value nodea) (value nodeb))
                   (generate-corridor (lchild nodea) (rchild nodea))
                   (generate-corridor (lchild nodeb) (rchild nodeb))))))))
;; (generate-corridor (rect 1 6 1 28) (rect 1 6 33 41)) ; => (3 3 29 32)
;; (generate-corridor (rect 1 6 33 41) (rect 1 6 1 28)) ; => (3 3 29 32)

(defmethod generate-corridors ((root btree-node))
  (alexandria:flatten (generate-corridor (lchild root) (rchild root))))

(defun %bsp-tree-area (leaves)
  (loop for leaf in leaves
        sum (area (value leaf))))

(defgeneric random-enemy-count (room)
  (:method ((rect rect))
    (random (ceiling (log (area rect))))))

(defun make-bsp-tree (boundary-rect isfinished &key parent)
  (if (funcall isfinished boundary-rect)
      (make-instance 'btree-node :parent parent
                                 :value boundary-rect)
      (let* ((direction (random-direction))
             (point
               (random-cutoff-point boundary-rect :include-upper-bound t)))
        (if
         (null point) ; no valid cutoff point => leaf
         (make-instance 'btree-node :parent parent
                                    :value boundary-rect)
         (multiple-value-bind (lchild-rect rchild-rect)
             (generate-child-rects boundary-rect point direction)
           (let ((skip-lchild (funcall isfinished lchild-rect))
                 (skip-rchild (funcall isfinished rchild-rect)))
             (if
              (or skip-lchild skip-rchild) ; no space left for 2 children => leaf
              (make-instance 'btree-node :parent parent
                                         :value boundary-rect)
              (let ((node (make-instance 'btree-node :value boundary-rect :parent parent)))
                (setf (lchild node) (make-bsp-tree lchild-rect isfinished :parent node)
                      (rchild node) (make-bsp-tree rchild-rect isfinished :parent node))
                node))))))))

(defun %bsp-shrink-leaves-f (leaves &key (pad-rect (rect 1 1 1 1)))
  (dolist (leaf leaves)
    (multiple-value-bind (padded valid)
        (rect-pad (value leaf) pad-rect)
      (when valid (setf (value leaf) padded))))
  leaves)

(defun %bsp-shrink-leaves (leaves &key (removep (lambda (leaf) (declare (ignore leaf)) nil))
                                    (pad-rect (rect 1 1 1 1)))
  (remove-if
   removep
   (mapcar
    (lambda (leaf)
      (multiple-value-bind (padded valid)
          (rect-pad leaf pad-rect)
        (if valid
            padded
            leaf)))
    leaves)))

(defun print-map (map)
  (format *debug-stream* "~{~a~^~%~}~%"
          (loop for row from 0 below (array-dimension map 0)
                collect
                (map 'string (lambda (c) c)
                     (make-array (array-dimension map 1)
                                 :displaced-to map
                                 :displaced-index-offset (* row (array-dimension map 1)))))))

(defun %bsp-map-insert-alist-values (map alist)
  (loop for (pos . entity) in alist do
    (setf (aref map (car pos) (cdr pos))
          entity)))

(defun %bsp-room-entity-mapper (map leaves mapper)
  (loop for leaf in leaves
        for room = (value leaf) do
          (%bsp-map-insert-alist-values map (funcall mapper room))))

(defun room-entity-mapper-by-level (level)
  (lambda (room)
    (labels
              ((recur-build-entity-alist (n &optional (i 0) alist)
                 (if (>= i n)
                     alist
                     (let ((position
                             (random-position
                              room
                              ;; random position is valid only if it's not already in alist
                              (lambda (pos) (null (assoc pos alist :test #'equal)))
                              :retries 30)))
                       (if (null position)
                           ;; since random-position failed, we assume there are no more
                           ;; valid positions in the room
                           alist
                           (recur-build-entity-alist n (1+ i) (acons position #\s alist)))))))
            (recur-build-entity-alist (random-enemy-count room)))))

(defun %rooms-postprocess-add-chest-and-stairs (map leaves)
  (flet ((free-pos-predicate (pos)
           (case (aref map (car pos) (cdr pos))
             (#\. t)
             (otherwise nil))))
    (let* ((shuffled (sort (copy-list leaves)
                           (lambda (a b)
                             (declare (ignore a b))
                             (zerop (random 2)))))
           (chest-pos (random-position (value (car shuffled)) #'free-pos-predicate))
           (upstair-pos (random-position (value (car (last shuffled))) #'free-pos-predicate)))
      (list (cons chest-pos #\$) (cons upstair-pos #\<)))))

(defmethod generate-map ((gen bsp-mapgen))
  (with-accessors ((rows rows) (cols cols) (min-height min-height) (min-width min-width))
      gen
    (loop do
      (let* ((root-node (make-bsp-tree (rect 0 (1- rows) 0 (1- cols))
                                       (lambda (rect)
                                         (destructuring-bind (height . width)
                                             (rect-dimensions rect :include-upper-bound t)
                                           (or (< height (min-height gen)) (< width (min-width gen)))))))
             (leaves (collect-leaves root-node))
             (map (make-array (list rows cols) :initial-element #\#)))
        (%bsp-shrink-leaves-f leaves)
        (loop for leaf in leaves do
          (with-slots (x1 x2 y1 y2)
              (value leaf)
            (loop for x from x1 upto x2 do
              (loop for y from y1 upto y2
                    do (setf (aref map x y) #\.)))))
        (let ((corridors
                (generate-corridors root-node))
              (start-pos
                (random-position
                 map (lambda (tile) (or (char= #\. tile) (char= #\= tile))))))
          (loop for corridor in corridors do
            (with-slots (x1 x2 y1 y2)
                corridor
              (loop for x from x1 upto x2 do
                (loop for y from y1 upto y2 do
                  (setf (aref map x y) #\=)))))
          (format *debug-stream* "CORRIDORS: ~a~%" corridors)
          (multiple-value-bind (distances path)
              (dijkstra map (lambda (v)
                              (case v
                                ((#\. #\=) 1)
                                (otherwise 1000)))
                        :src start-pos)
            (declare (ignorable distances))
            (labels ((add-missing-corridors (prev)
                       (when prev
                         (destructuring-bind (row . col)
                             prev
                           (when (char= (aref map row col) #\#)
                             (setf (aref map row col) #\=))
                           (add-missing-corridors (aref path row col))))))
              (dolist (leaf-center
                       (mapcar
                        (lambda (leaf) (center-of (value leaf)))
                        leaves))
                (add-missing-corridors (aref path (car leaf-center) (cdr leaf-center)))))))
        (%bsp-room-entity-mapper map leaves (room-entity-mapper gen))
        (%bsp-map-insert-alist-values map (funcall (rooms-postprocess gen) map leaves))
        (print-map map)
        (let* ((rooms (length leaves))
               (area (%bsp-tree-area leaves))
               (ratio (/ area (* rows cols))))
          (if (or (< rooms (min-rooms gen))
                  (< ratio (min-area% gen)))
              (format *debug-stream* "Constraints not fullfilled: ROOMS ~a, AREA ~a, RATIO ~f~%Rerunning algorithm...~%"
                      rooms area ratio)
              (return map)))))))

(defmethod map-to-level ((store level-store) map depth to-tile)
  "MAP is 2D array with characters or symbols as elements. TO-TILE is a
function (row col depth character) => ENTITY (tile), which is applied to each
element in MAP, replacing it. Finally MAP is added to STORE at DEPTH."
  (with-matrix-indices (row col) map
    (setf (aref map row col)
          (funcall to-tile row col depth (aref map row col))))
  (add-level store depth :level-matrix map))

(defmethod map-add-entities ((store entity-store) map depth to-entity)
  "MAP is 2D array with characters or symbols as elements. TO-ENTITY is a function
with parameters (row col depth element-at-position) => ENTITY. Non-nil return
values from TO-ENTITY are added to STORE."
  (with-matrix-indices (row col) map
    (let ((maybe-entity (funcall to-entity row col depth (aref map row col))))
      (when maybe-entity
        (add-entity store maybe-entity)))))

(defun generate-map-with-preview (mapgen)
  (format *debug-stream* "Generating maps. Press space to generate a new map or Q to quit.")
  (let ((levels (make-instance 'level-store)))
    (flet
        ((generate-new-map ()
           (let ((map (generate-map mapgen))
                 (ctr 0))
             (map-to-level
              levels map 0
              (lambda (row col depth char)
                (make-instance 'entity :posx row :posy col :posz depth :id (incf ctr)
                                       :look
                                       (case char
                                         (#\# 'sprite-wall-black-bg)
                                         (#\. 'sprite-dot)
                                         (#\= 'sprite-floor-bright-brown)
                                         (#\s 'sprite-snake)
                                         (otherwise 'sprite-undefined))))))))
      (generate-new-map)
      (ui-toplevel :level-store levels
                   :handle-inputs (lambda (store input)
                                    (declare (ignore store))
                                    (case input
                                      (dwim
                                       (generate-new-map))))))))
