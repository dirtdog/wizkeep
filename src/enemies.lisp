;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defvar *actor-prefabs* (make-hash-table))

(defun make-actor (prefab-symbol)
  (funcall (gethash prefab-symbol *actor-prefabs*)))

(defmacro defactor (prefab-symbol (&rest defaults-plist))
  "Return a function to generate an actor with the configuration provided in
DEFAULTS-PLIST. Additionally, the function is stored in
*ACTOR-PREFABS*. DEFAULTS-PLIST keys match initars keys of ENTITY, except
:TRAITS becomes :TRAITS-PLIST. :COLLISION, :LOOK, and :TRAITS-PLIST initargs are
set by default if not provided."
  (let ((initargs-plist
          (loop for (k v) on defaults-plist by #'cddr
                append (case k
                         ((:traits traits)
                          (list :traits-plist
                                (if (member 'hp defaults-plist)
                                    `(list ,@v)
                                    `(list ,@v 'hp 1))))
                         (otherwise
                          (list (intern (symbol-name k) :keyword) v))))))
    (setf initargs-plist
          (append initargs-plist
                  (unless (member :look initargs-plist)
                    '(:look 'sprite-monster-default))
                  (unless (member :collision initargs-plist)
                    '(:collision t))
                  (unless (member :traits-plist initargs-plist)
                    '(:traits-plist (hp 1)))))
    (when (eq 'undefined (getf initargs-plist :collision 'undefined))
      (setf initargs-plist (append '(:collision t) initargs-plist)))
    `(setf (gethash ',prefab-symbol *actor-prefabs*)
           (lambda ()
             (make-instance 'entity ,@initargs-plist)))))

(defun loot-table (alist &key (n 100))
  "Returns a list of items. Each pair in ALIST contains (item . rng-limit). For
 each pair, if (random N) exceeds rng-limit, item is returned as part of the
 result."
  (loop for (item . lim) in alist
        when (> (random n) lim)
          collect item))

(defparameter *snake-loot-table*
  '((sprite-paren-left . 15)
    (sprite-paren-right . 15)
    (sprite-key-silver . 20)))

(defparameter *only-key-loot-table*
  '((sprite-key-silver . 20)))

(defactor snake (:look 'sprite-snake
                 :traits ('hp 5 'speed 13 'current-weapon *weapon-snake* 'name "SNAKE"
                              'loot-table
                              (lambda ()
                                (loot-table *snake-loot-table*)))))

(defactor coffee-recipe (:look 'sprite-script
                         :traits ('hp 3 'speed 10 'current-weapon *weapon-callback* 'name "COFFEE RECIPE")))

(defactor crab (:look 'sprite-crab
                :traits ('hp 8 'speed 8 'current-weapon *weapon-crab-claw* 'name "OXIDATED CRAB"
                             'loot-table
                             (lambda ()
                               (loot-table *only-key-loot-table*)))))

(defactor enemy-factory (:look 'sprite-factory
                         :traits ('hp 18 'speed 20 'current-weapon *weapon-spring-boot*
                                      'name "CoffeeFactory")))

(defactor scheming-wizard (:look 'sprite-scheming-wizard
                           :traits ('hp 20 'speed 10 'name "SCHEMING WIZARD")))

;; => action (e.g. pass-turn, move-to, throw-item, range-attack)
(defun simple-monster-ai (path failp successp)
  "Walks the list of cons-position pairs PATH. If FAILP evaluates to non-nil, that
value is returned. If SUCCESSP evaluates to nin-nil, a rect with the first
position of the PATH and the succeeding position is returned. If neither
function succeeded for any element of the path, 'INVALID-PATH is returned. FAILP
is always evaluated before SUCCESSP.

FAILP and SUCCESSP must have the following function signatures:
(steps row col) => value"
  (labels
      ((walk-path (subpath &optional (steps 1))
         (format *debug-stream* "WALK-PATH ~a steps " steps)
         (if (null subpath)
             (progn
               (format *debug-stream* "- no valid path~%")
               'invalid-path)
             (destructuring-bind (row . col)
                 (car subpath)
               (format *debug-stream* "- checking (~a ~a)~%" row col)
               (cond
                 ((or (funcall failp steps row col)))
                 ((funcall successp steps row col)
                  (format *debug-stream* "  success! commiting~%")
                  ;; positions of first and final element in the path
                  (rect (caar path) row (cdar path) col))
                 (t
                  (walk-path (cdr subpath) (1+ steps))))))))
    (walk-path path)))

(defun path-from-dijkstra-map (map row col)
  "Recursively walks the 2D-array Dijkstra map MAP, starting from (ROW COL), and
 returns the path to the destination."
  (let ((prev (aref map row col)))
    (if (null prev)
        nil
        (cons prev (path-from-dijkstra-map map (car prev) (cdr prev))))))

(defun %test-simple-ai ()
  (let ((prev (nth-value 1 (dijkstra (make-array '(5 5))
                                     (lambda (v)
                                       (declare (ignorable v)) 1)
                                     :src '(4 . 4)))))
    (flet ((failp (steps row col)
             (declare (ignorable steps row col))
             (when (> steps 10)
               (format *debug-stream* "  fail. aborting~%")
               'pass-turn))
           (successp (steps row col)
             (declare (ignorable steps row col))
             (equal (list row col) '(4 4)))
           (on-commit (monster rect)
             (declare (ignorable monster rect))
             (with-slots ((newx x1) (newy y1)) rect
               (if (and (= newx 4) (= newy 4))
                   'attacking
                   (progn
                     (setf (posx monster) newx
                           (posy monster) newy)
                     'moving)))))
      (let ((monster (make-instance 'entity))
            (path (path-from-dijkstra-map prev 0 0)))
        (format *debug-stream* "ALGORITHM START~%")
        (loop for rest on path do
          (let ((result (simple-monster-ai rest #'failp #'successp)))
            (cond
              ((eq 'pass-turn result)
               (return result))
              (t
               (case (on-commit monster result)
                 (moving (format *debug-stream* "MONSTER moved~%"))
                 (otherwise (return result)))))))))))
