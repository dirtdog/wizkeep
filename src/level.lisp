;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass level-store ()
  ((levels :accessor levels
           :initarg :levels
           :initform (make-hash-table)
           :documentation "Hash table. Key is depth (integer), value is 2D array with level state.")
   (depth :accessor depth
          :initform 0
          :documentation "Key to current level.")
   (rows :accessor rows
         :initarg :rows
         :initform 32
         :documentation "Number of rows per level.")
   (cols :accessor cols
         :initarg :cols
         :initform 48
         :documentation "Number of cols per level.")))

(defparameter *level-rows* 24)
(defparameter *level-cols* 32)
(defvar *level-store* (make-instance 'level-store))

(defmethod reset ((store level-store))
  (setf (levels store) (make-hash-table)
        (depth store) 0))

(defmethod add-level ((store level-store) depth &key level-matrix rows cols)
  (setf
   (gethash depth (levels store))
   (if level-matrix
       level-matrix
       (let ((rows (if rows rows (rows store)))
             (cols (if cols cols (cols store))))
         (make-array (list rows cols) :initial-element nil)))))

(defmethod current-level ((store level-store))
  (gethash (depth store) (levels store)))

(defmethod add-entity ((store level-store) (entity entity))
  (setf (aref (gethash (posz entity) (levels store))
              (posx entity) (posy entity))
        entity))

(defmethod move-entity ((store level-store) from-row from-col to-row to-col &key from-level to-level)
  (restart-case
      (let* ((from-level (if from-level from-level (depth store)))
             (to-level (if to-level to-level from-level)))
        (when (or (< to-row 0) (>= to-row (rows store))
                  (< to-col 0) (>= to-col (cols store))
                  (null (gethash from-level (levels store)))
                  (null (gethash to-level (levels store))))
          (error "invalid index"))
        (let ((entity (shiftf (aref (gethash from-level (levels store)) from-row from-col)
                              nil)))
          (setf (posx entity) to-row
                (posy entity) to-col
                (posz entity) to-level
                (aref (gethash to-level (levels store)) to-row to-col) entity)))
    (do-nothing () nil)))

(defmethod move ((store level-store) (entity entity) direction)
  (flet ((move-with-offset (row col)
           (let ((posx (posx entity))
                 (posy (posy entity)))
             (move-entity store posx posy (+ row posx) (+ col posy)))))
    (apply #'move-with-offset (direction-list direction))))

(defgeneric random-position (place predicate &key retries)
  (:method ((store level-store) predicate &key retries)
    (loop for i from 0 do
      (when (and retries (> i retries))
        (error "retries exceeded")) ; TODO restart
      (let ((row (random (rows store)))
            (col (random (cols store))))
        (when (funcall predicate (aref (current-level store) row col))
          (return (cons row col))))))
  (:method ((map array) predicate &key retries)
    (loop for rows = (array-dimension map 0)
          for cols = (array-dimension map 1)
          for i from 0 do
            (let ((row (random rows))
                  (col (random cols)))
              (if (funcall predicate (aref map row col))
                  (return (cons row col))
                  (progn
                    (when (and retries (> i retries))
                      (error "retries exceeded")))))))
  (:method ((room rect) predicate &key retries)
    (destructuring-bind (height . width)
        (rect-dimensions room :include-upper-bound t)
      (loop for i from 0 do
        (when (and retries (> i retries))
          (error "retries exceeded")) ; TODO restart
        (let ((pos
                (cons (+ (random height) (rect-x1 room))
                      (+ (random width) (rect-y1 room)))))
          (when (funcall predicate pos)
            (return pos)))))))
