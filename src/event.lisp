;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defparameter *event-stream* (make-array 1024 :fill-pointer t :element-type 'character :adjustable nil))

(defclass event ()
  ((actor :accessor actor
          :initarg :actor)))

(defclass move-event (event) ())

(defclass pass-turn-event (event) ())

(defclass gc-triggered-event (event) ())

(defclass gc-completed-event (event) ())

(defclass attack-event (event)
  ((victim :accessor victim
           :initarg :victim)
   (damage :reader damage
           :initarg :damage)
   (weapon :accessor weapon
           :initarg :weapon)))

(defclass actor-died-event (event)
  ((drops :accessor drops
          :initarg :drops
          :initform nil)))

(defclass prop-collect-event (event)
  ((outcome :accessor outcome
            :initarg :outcome
            :initform nil
            :documentation "Non-NIL means action was performed.")
   (actor :accessor actor
          :initarg :actor
          :initform nil
          :documentation "Actor that collects the prop.")
   (props :accessor props
          :initarg :props
          :initform nil
          :documentation "Prop ENTITY or list of prop entities.")))

(defmethod id ((event event))
  (id (actor event)))

(defgeneric event-string (event)
  (:method (event)
    "")
  (:method ((event attack-event))
    (with-slots (actor victim damage weapon) event
      (let ((verb (and (weapon event) (weapon-verb weapon victim))))
        (if verb
            (format nil "~a ~a for ~a." (name actor) verb damage)
            (format nil "~a hits ~a for ~a." (name actor) (name victim) damage)))))
  (:method ((event actor-died-event))
    (let ((drops (drops event)))
      (if drops
          (let ((drops-string
                  (if (listp drops)
                      (serapeum:string-join
                       (mapcar (lambda (prop) (description prop)) drops)
                       ", ")
                      (description drops))))
            (format nil "~a died, dropping ~a." (name (actor event)) drops-string))
          (concatenate 'string (name (actor event)) " died."))))
  (:method ((event prop-collect-event))
    (with-slots (actor outcome props) event
      (flet ((pick-up-prop-string (prop)
               (format nil "~a picked up ~a." (name actor) (description prop))))
        (if outcome
            (cond
              ((eq outcome 'opened)
               (format nil "~a opened ~a." (name actor) (description props)))
              ((prop-p outcome)
               (pick-up-prop-string props))
              ((and outcome (listp outcome))
               (serapeum:string-join (mapcar #'pick-up-prop-string props) " "))
              (t ""))
            ""))))
  (:method ((event gc-triggered-event))
    "You feel a compulsion to collect garbage!")
  (:method ((event gc-completed-event))
    "So tidy! You feel refreshed."))
