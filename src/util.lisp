;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defmacro with-matrix-indices ((row col) array2d &body body)
  `(loop for ,row below (array-dimension ,array2d 0) do
    (loop for ,col below (array-dimension ,array2d 1) do
          ,@body)))

(defmacro with-matrix-indices-below ((row rows col cols) &body body)
  `(loop for ,row below ,rows do
    (loop for ,col below ,cols do
          ,@body)))

(defclass dice ()
  ((sides :reader sides
          :initarg :sides
          :initform 2)
   (rolls :reader rolls
          :initarg :rolls
          :initform 1)
   (bonus :reader bonus
          :initarg :bonus
          :initform 0
          :documentation "Constant value added after summing dice rolls.")))

(defun d (sides &optional (rolls 1) (bonus 0))
  (make-instance 'dice :sides sides :rolls rolls :bonus bonus))

(defmethod roll-dice ((dice dice))
  (+ (* (rolls dice) (1+ (random (sides dice))))
     (bonus dice)))

(defparameter *debug-stream* nil)
(defvar *up* '(-1 0))
(defvar *upleft* '(-1 -1))
(defvar *upright* '(-1 1))
(defvar *left* '(0 -1))
(defvar *right* '(0 1))
(defvar *down* '(1 0))
(defvar *downleft* '(1 -1))
(defvar *downright* '(1 1))
(defvar *directions* '(up upleft upright left right down downleft downright))
(defvar *directions4* '(up left right down))

(defun direction-list (symbol)
  (case symbol
    (up *up*)
    (upleft *upleft*)
    (upright *upright*)
    (left *left*)
    (right *right*)
    (down *down*)
    (downleft *downleft*)
    (downright *downright*)))

(defun random-direction (&optional (directions 2))
  "Returns 'HORIZONTAL or 'VERTICAL."
  (case directions
    (2
     (if (zerop (random 2))
         'horizontal
         'vertical))
    (4
     (case (random 4)
       (0 'up)
       (1 'left)
       (2 'right)
       (3 'down)))
    (8
     (case (random 8)
       (0 'up)
       (1 'upleft)
       (2 'upright)
       (3 'left)
       (4 'right)
       (5 'down)
       (6 'downleft)
       (7 'downright)))
    (otherwise
     (error "unsupported number of directions"))))

(defgeneric shift-position-list (position direction)
  (:method (position (direction symbol))
    (destructuring-bind (dx dy) (direction-list direction)
      (list (+ (car position) dx)
            (+ (cadr position) dy))))
  (:method (position (offset list))
    (list (+ (car position) (car offset))
          (+ (cadr position) (cadr offset)))))

(defgeneric shift-position-pair (position direction)
  (:method (position (direction symbol))
    (destructuring-bind (dx dy) (direction-list direction)
      (cons (+ (car position) dx)
            (+ (cdr position) dy))))
  (:method (position (offset list))
    (cons (+ (car position) (car offset))
          (+ (cdr position) (cadr offset)))))

(defun one-of (&rest items)
  (when items (nth (random (length items)) items)))

;; see https://lispcookbook.github.io/cl-cookbook/error_handling.html
(defun prompt-new-value (prompt)
  (format *query-io* prompt) ;; *query-io*: the special stream to make user queries.
  (force-output *query-io*)  ;; Ensure the user sees what he types.
  (list (read *query-io*)))  ;; We must return a list.
