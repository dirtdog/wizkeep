;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defparameter *window-height* 1200)
(defparameter *window-width* 1200)
(defparameter *pixelformat* sdl2:+pixelformat-unknown+)
(defparameter *tileset-path* (uiop:native-namestring "assets/urizen_onebit_tileset__v1d0.png"))
(defvar *urizen-sprites* (make-hash-table))
(defvar *urizen-ascii* (make-hash-table :test 'equal))

(defun sprite (x y w &optional h)
  (sdl2:make-rect x y w (if h h w)))

(defmethod add-sprite (store name sprite &key (free-old t))
  (when free-old
    (let ((old (gethash name store)))
      (when old
        (remhash name store)
        (sdl2:free-rect old))))
  (setf (gethash name store) sprite))

(defmethod remove-sprite (store name)
  (let ((to-remove (gethash name store)))
    (remhash name store)
    (sdl2:free-rect to-remove)))

(defmethod flush-sprites (store)
  (maphash (lambda (k v)
             (declare (ignore v))
             (remove-sprite store k))
           store))

(defgeneric presentation (spritesheet entity)
  (:method (spritesheet none)
    (gethash 'sprite-dot *urizen-sprites*))
  (:method (spritesheet (symbol symbol))
    (gethash symbol *urizen-sprites*))
  (:method (spritesheet (char character))
    (gethash char *urizen-ascii*))
  (:method (spritesheet (entity entity))
    (gethash (look entity) *urizen-sprites*)))

(add-sprite *urizen-sprites* 'sprite-at (sprite 1054 612 12))
(add-sprite *urizen-sprites* 'sprite-crab (sprite 92 235 12))
(add-sprite *urizen-sprites* 'sprite-snake (sprite 27 235 12))
(add-sprite *urizen-sprites* 'sprite-dot (sprite 1 66 12))
(add-sprite *urizen-sprites* 'sprite-undefined (sprite 1327 638 12))
(add-sprite *urizen-sprites* 'sprite-wall-black-bg (sprite 1015 27 12))
(add-sprite *urizen-sprites* 'sprite-corridor (sprite 79 79 12))
(add-sprite *urizen-sprites* 'sprite-floor-bright-brown (sprite 183 2 12))
(add-sprite *urizen-sprites* 'sprite-monster-default (sprite 338 118 12))
(add-sprite *urizen-sprites* 'sprite-gravestone (sprite 92 508 12))
(add-sprite *urizen-sprites* 'sprite-script (sprite 352 430 12))
(add-sprite *urizen-sprites* 'sprite-green-alien (sprite 1028 157 12))
(add-sprite *urizen-sprites* 'sprite-toilet (sprite 782 261 12))
(add-sprite *urizen-sprites* 'sprite-factory (sprite 703 79 12))
(add-sprite *urizen-sprites* 'sprite-seer (sprite 430 118 12))
(add-sprite *urizen-sprites* 'sprite-seer-peach (sprite 339 508 12))
(add-sprite *urizen-sprites* 'sprite-paren-left (sprite 1171 508 12))
(add-sprite *urizen-sprites* 'sprite-paren-right (sprite 1145 508 12))
(add-sprite *urizen-sprites* 'sprite-stair-down (sprite 300 14 12))
(add-sprite *urizen-sprites* 'sprite-stair-up (sprite 313 14 12))
(add-sprite *urizen-sprites* 'sprite-chest-closed (sprite 391 443 12))
(add-sprite *urizen-sprites* 'sprite-chest-open (sprite 352 443 12))
(add-sprite *urizen-sprites* 'sprite-table (sprite 14 612 12))
(add-sprite *urizen-sprites* 'sprite-book (sprite 508 430 12))
(add-sprite *urizen-sprites* 'sprite-gun-space (sprite 1015 79 12))
(add-sprite *urizen-sprites* 'sprite-shield-bronze (sprite 352 586 12))
(add-sprite *urizen-sprites* 'sprite-goggles (sprite 820 326 12))
(add-sprite *urizen-sprites* 'sprite-key-silver (sprite 40 586 12))
(add-sprite *urizen-sprites* 'sprite-bookcase (sprite 27 521 12))
(add-sprite *urizen-sprites* 'sprite-heart (sprite 1015 482 12))
(add-sprite *urizen-sprites* 'sprite-scheming-wizard (sprite 469 131 12))

(defun load-urizen-alphabet (store)
  (flet ((string-to-tiles (string first-x y &optional (xby 13))
           (loop for char across string
                 for n from first-x by xby
                 do (add-sprite store char (sprite n y (1- xby))))))
    (string-to-tiles "ABCDEFGHIJKLMNOPQRST12345" 1015 573)
    (string-to-tiles "UVWXYZabcdefghijklmn67890" 1015 586)
    (string-to-tiles "opqrstuvwxyz()[]{}<>+-?!^" 1015 599)
    (string-to-tiles ":#_@%~$\"'&*=`|/\\.,;" 1015 612)))

(defun %get-ascii-string (first-char last-char)
  (map 'string #'identity
       (loop for char from (char-code first-char) upto (char-code last-char)
             collect (code-char char))))

(defun load-texture (renderer pathname)
  (let ((surface (sdl2-image:load-image pathname)))
    (prog1 (sdl2:create-texture-from-surface renderer surface)
      (sdl2:free-surface surface))))

(defun %random-sprite ()
  (let* ((keys (alexandria:hash-table-keys *urizen-sprites*))
         (rand (random (length keys))))
    (nth rand keys)))

(defun %sprite-dims (base-height base-width &key (rows *level-rows*) (cols *level-cols*) (increment (/ 1 2)))
  "Returns the highest (HEIGHT WIDTH) pair that fits within the window given
 ROWS and COLS, and window height/width."
  (let ((max-height (/ *window-height* rows))
        (max-width (/ *window-width* cols)))
    (loop for multiplier from 1 by increment do
      (let ((next-mult (+ multiplier increment)))
        (when (or (> (* base-height next-mult) max-height)
                  (> (* base-width next-mult) max-width))
          (return-from %sprite-dims (list (* base-height multiplier) (* base-width multiplier))))))))

(defun ui-render-copy-tiles (entity-store level-store rows cols copy-fn &key game-over)
  (if game-over
      (with-matrix-indices-below (x rows y cols)
        (funcall copy-fn x y 'sprite-gravestone))
      (let* ((depth (depth level-store))
             (visible (visible-actors entity-store :depth depth)))
        (with-matrix-indices-below (x rows y cols)
          (let* ((actor
                   (find-if (lambda (e)
                              (and e
                                   (= x (posx e))
                                   (= y (posy e))
                                   (= depth (posz e))))
                            visible))
                 (presentation
                   (if actor
                       ;; draw visible actor
                       actor
                       ;; draw background tile
                       (aref (current-level level-store) x y))))
            (funcall copy-fn x y presentation))))))

(defparameter *sdl-scancode-alist*
  '((:scancode-h . left)
    (:scancode-j . down)
    (:scancode-k . up)
    (:scancode-l . right)
    (:scancode-y . upleft)
    (:scancode-u . upright)
    (:scancode-b . downleft)
    (:scancode-n . downright)
    (:scancode-space . dwim)
    (:scancode-escape . quit)
    (:scancode-q . quit)))

(defun scancode-to-action (scancode)
  (let ((cons (assoc scancode *sdl-scancode-alist* :test #'sdl2:scancode=)))
    (when cons
      (cdr cons))))

(defun %sdl-dummy-handle-input (store input)
  (declare (ignore store input))
  (format t "Not handling inputs! Press Q to close window.~%"))

(defun ui-string-to-sprite (string)
  (loop for char across string
        collect (gethash char *urizen-ascii*)))

(defun %int-to-char-list (int)
  (map 'list (lambda (c) c)
       (format nil "~a" int)))

(defun ui-player-status-bar (player)
  ;; hp gc cons
  (let ((inventory (trait player 'inventory)))
    `(sprite-heart
      nil
      ;; ,@(ui-string-to-sprite (format nil "~a/~a" (trait player 'hp) (trait player 'max-hp)))
      ,@(map 'list #'identity (format nil "~a/~a" (trait player 'hp) (trait player 'max-hp)))
      nil nil
      sprite-toilet
      ;; ,@(ui-string-to-sprite (format nil "~a/~a" (gc-ctr player) (gc-ctr-max player)))
      ,@(map 'list #'identity (format nil "~a/~a" (gc-ctr player) (gc-ctr-max player)))
      nil
      sprite-paren-left
      ,@(%int-to-char-list (count-if (lambda (item) (eq (look item) 'sprite-paren-left)) inventory))
      sprite-dot
      ,@(%int-to-char-list (count-if (lambda (item) (eq (look item) 'sprite-paren-right)) inventory))
      sprite-paren-right
      nil
      ;; key current level
      ,(when (find-if (lambda (item)
                        (and (eq (look item) 'sprite-key-silver)
                             (= (posz *player*) (posz item))))
                      inventory)
         'sprite-key-silver))))


(defclass line-printer ()
  ((height :accessor height
           :initarg :height)
   (width :accessor width
          :initarg :width)
   (row :accessor row
        :initform 0)
   (col :accessor col
        :initform 0)
   (lines :accessor lines
          :initarg :lines)))

(defmethod initialize-instance :after ((printer line-printer) &key)
  (unless (and (slot-boundp printer 'lines)
               (lines printer))
    (setf (lines printer) (make-array (height printer) :initial-element ""))))

(defmethod flush ((printer line-printer))
  (loop for i below (length (lines printer)) do
        (setf (aref (lines printer) i) "")))

(defmethod go-next-row ((printer line-printer))
  (with-slots (row col height) printer
    (if (>= (1+ row) height)
        (progn
          (setf row 0 col 0)
          (flush printer))
        (setf row (1+ row)
          col 0))))

(defmethod write-word ((printer line-printer) word &key space)
  (with-slots (row col width lines) printer
    (when (>= (+ (length word) col) width)
      (go-next-row printer))
    (incf col
          (length
           (setf (aref lines row)
                 (concatenate 'string
                              (aref lines row)
                              word
                              (if (and space
                                       (not (>= (+ (length word) col)
                                                (1- width))))
                                  " "
                                  "")))))))

(defmethod printer-push-string ((printer line-printer) string)
  ;; Scenario:
  ;; 4 height, 6 width
  ;; *****
  ;; *****
  ;; ***@* ; row = 2, col 4
  ;; *****
  ;; =>
  ;; max string: 6 + 2
  ;; width * (height - (row + 1)) + width - col
  ;; must also check if first word exceeds line => skip (+ (- width col))
  (with-slots (height width row col lines)
      printer
    (incf col)
    (let ((spaces (count #\  string))
          (max-chars-first-line (- width col))
          (first-word-len (position #\  string)))
      (when (> (+ spaces
                  (length string))
               (* height width))
        (warn "Skipping string (~a exceeds length buffer length.)" (length string))
        (return-from printer-push-string))
      (when (and (null first-word-len)
                     (> (length string) max-chars-first-line)))
      (when (and first-word-len
             (> first-word-len max-chars-first-line))
        (go-next-row printer))
      (let ((remaining-space (+ (* width (- height (1+ row)))
                                (- width col))))
        (when (< remaining-space (length string))
          (go-next-row printer)))
      (labels ((enter-string (substr)
                 (unless (zerop (length substr))
                   (let ((split (position #\  substr)))
                     (cond
                       ((not split)
                        (write-word printer substr))
                       (t
                        (write-word printer (subseq substr 0 split) :space t)
                        (enter-string (subseq substr (1+ split)))))))))
        (enter-string string)))))

(defun ui-toplevel (&key (entity-store *entity-store*) (level-store *level-store*)
                      (handle-inputs #'%sdl-dummy-handle-input)
                      run-turns)
  ;; 25 + 25 + 25 + 19 (number of characters in the tileset)
  (when (< (hash-table-count *urizen-ascii*) 94)
    (load-urizen-alphabet *urizen-ascii*))
  (sdl2:with-init (:video)
    (sdl2:with-window (window :title "Wizard's Keep"
                              :w *window-width*
                              :h *window-height*
                              :flags '(:shown))
      (sdl2:with-renderer (renderer window :flags '(:accelerated))
        (let* ((tileset (load-texture renderer *tileset-path*))
               (*pixelformat* (sdl2:query-texture tileset))
               (level-rows (rows level-store))
               (level-cols (cols level-store))
               (num-status-rows 1)
               (num-log-rows 5)
               (line-printer (make-instance 'line-printer :height num-log-rows :width (cols level-store)))
               (total-rows (+ num-status-rows num-log-rows level-rows))
               (sprite-dim (%sprite-dims 12 12 :rows total-rows :cols level-cols))
               (rects
                 (let ((rects (make-array (list total-rows level-cols))))
                   (with-matrix-indices-below (row total-rows col level-cols)
                     (setf (aref rects row col)
                           ;; SDL coordinate system has X-axis going eastwards
                           ;; and Y-axis going southwards
                           (sdl2:make-rect (* col (first sprite-dim)) (* row (second sprite-dim))
                                           (second sprite-dim) (first sprite-dim))))
                   rects))
               (status-row
                 (make-array (list num-status-rows (array-dimension rects 1))
                             :displaced-to rects :displaced-index-offset (* level-rows (array-dimension rects 1))))
               (log-rows
                 (make-array (list (- total-rows level-rows 1) (array-dimension rects 1))
                             :displaced-to rects :displaced-index-offset (* (+ num-status-rows level-rows) (array-dimension rects 1))))
               (canvas (sdl2:create-texture renderer *pixelformat* sdl2-ffi:+sdl-textureaccess-target+ *window-width* *window-height*))
               (game-over nil))
          (unwind-protect
               (sdl2:with-event-loop (:method :poll)
                 (:keyup (:keysym keysym)
                         (let ((action (scancode-to-action (sdl2:scancode-value keysym))))
                           (case action
                             (quit (sdl2:push-event :quit))
                             (otherwise (funcall handle-inputs entity-store action)))))
                 (:idle
                  ()
                  (when (and (not game-over)
                             run-turns)
                    (when (eq (funcall run-turns) 'game-over)
                      (setf game-over t)))
                  (sdl2:set-render-target renderer canvas)
                  (sdl2:set-render-draw-color renderer 0 0 0 255)
                  (sdl2:render-clear renderer)
                  (ui-render-copy-tiles
                   entity-store level-store level-rows level-cols
                   (lambda (row col presentation)
                     (sdl2:render-copy renderer tileset
                                       :dest-rect (aref rects row col)
                                       :source-rect (presentation *urizen-sprites* presentation)))
                   :game-over game-over)
                  ;; log window
                  (if (not game-over)
                      (unless (zerop (length *event-stream*))
                        (printer-push-string line-printer *event-stream*)
                        (setf (fill-pointer *event-stream*) 0))
                      (progn ; game over
                        (flush line-printer)
                        (printer-push-string line-printer "Game over!")))
                  (loop for string across (lines line-printer)
                        for line-number from 0
                        do
                           (loop for char across string
                                 for y from 0 do
                                   (case char
                                     ((#\ ) nil)
                                     (otherwise
                                      (sdl2:render-copy renderer tileset :dest-rect (aref log-rows line-number y)
                                                                         :source-rect (presentation *urizen-ascii* char))))))
                  ;; status bar
                  (unless game-over
                    (loop for sprite in (ui-player-status-bar (player entity-store))
                          for y from 10
                          do
                             (when sprite
                               (sdl2:render-copy
                                renderer tileset
                                :dest-rect (aref status-row 0 y)
                                :source-rect
                                (presentation (if (characterp sprite) *urizen-ascii* *urizen-sprites*) sprite)))))
                  (sdl2:set-render-target renderer nil)
                  (sdl2:render-copy renderer canvas)
                  (sdl2:render-present renderer)
                  (sdl2:delay 33))
                 (:quit () t))
            (progn
              (sdl2:destroy-texture tileset)
              (sdl2:destroy-texture canvas)
              (with-matrix-indices (row col) rects
                (sdl2:free-rect (aref rects row col))))))))))
