;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass gc-entity (entity)
  ((gc-ctr :accessor gc-ctr
           :initform 0)
   (gc-ctr-max :accessor gc-ctr-max
               :initform 6)
   (gc-delay :accessor gc-delay
             :initform 2)
   (did-gc :accessor did-gc
           :initform nil)))

(defclass player (gc-entity)
  ;; ENTITY default overrides
  ((collision :initform t)
   (fov :initform 6)
   (look :initform 'sprite-green-alien)
   (traits :initform
           (alexandria:plist-hash-table
            (list 'hp 20
                  'max-hp 20
                  'delay 0
                  'speed 10
                  'current-weapon *weapon-cons*
                  'name "L.A." 'collector t)))))

(defgeneric trigger-gc (entity)
  (:documentation
   "Returns turn delay if GC occurred for ENTITY, and updates GC state in this case.")
  (:method ((entity gc-entity))
    (if (>= (gc-ctr entity) (gc-ctr-max entity))
        (progn
          (setf (gc-ctr entity) 0
                (did-gc entity) t)
          (* (gc-delay entity)
             (trait entity 'speed 1)))
        0))
  (:method :around ((player player))
    (let ((delay (call-next-method player)))
      (unless (zerop delay)
        (handle-event *event-handler* (make-instance 'gc-triggered-event :actor player)))
      delay)))

(defgeneric before-actor-turn (actor)
  (:method ((entity entity))
    nil)
  (:method ((player player))
    (when (did-gc player) ; restore HP if last turn was GC
      (set-trait player 'hp (trait player 'max-hp))
      (setf (did-gc player) nil)
      (handle-event *event-handler* (make-instance 'gc-completed-event :actor player)))))

(defgeneric after-actor-turn (actor action)
  (:method ((entity entity) action)
    nil)
  (:method ((entity gc-entity) action)
    (case action
      (attack
       (let ((weapon (trait entity 'current-weapon nil)))
         (incf (gc-ctr entity)
               (if weapon (weight weapon) 1))))
      (otherwise
       (error "Unkown action ~a, skipping." action)
       nil))))

(defparameter *player* (make-instance 'player))
