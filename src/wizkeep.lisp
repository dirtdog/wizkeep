;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defvar *event-handler* (make-instance 'scheduler))
(defvar *entity-store* (make-instance 'entity-store :event-handler *event-handler*))
(defvar *input-handler* (make-instance 'input-handler))

(defun initialize-player ()
  (setf *player* (make-instance 'player))
  (add-entity *entity-store* *player*))

(defun walkable-neighbor-positions (entity)
  (with-slots (posx posy posz) entity
    (loop for (dx dy) in '((-1 0) (0 -1) (0 1) (1 0))
          when (not (collision
                     (aref (current-level *level-store*)
                           (+ posx dx) (+ posy dy))))
            collect (list (+ posx dx)
                          (+ posy dy)
                          (depth *level-store*)))))

(defun %simple-ai-failp (steps row col)
  (declare (ignorable steps row col))
  (when (> steps 12)
    'pass-turn))

(defun %simple-ai-successp (steps row col)
  (declare (ignorable steps row col))
  (and (= row (posx *player*))
       (= col (posy *player*))))

(defun %generate-level-add-tiles (row col depth char)
  (case char
    ((#\# #\= #\. #\< #\$)
     (add-entity *entity-store*
                 (make-instance
                  'entity :posx row :posy col :posz depth
                  :collision (char= char #\#)
                  :look
                  (case char
                    (#\# 'sprite-wall-black-bg)
                    ((#\. #\=) 'sprite-dot)
                    (otherwise 'sprite-dot)))))
    (otherwise
     (add-entity *entity-store*
                 (make-instance
                  'entity :posx row :posy col :posz depth
                  :collision (char= char #\#)
                  :look
                  'sprite-dot)))))

(defun %generate-level-to-entity (row col depth char)
  (let ((maybe-entity
          (case char
            (#\s
             (case depth
               (0 (make-actor (one-of 'snake)))
               (1 (make-actor (one-of 'snake 'coffee-recipe)))
               ((2 3) (make-actor (one-of 'snake 'coffee-recipe 'enemy-factory)))
               (4 (make-actor (one-of 'snake 'coffee-recipe 'crab 'enemy-factory)))
               (otherwise
                (make-actor (one-of 'scheming-wizard 'snake 'coffee-recipe 'crab 'enemy-factory)))))
            (#\<
             (make-staircase row col depth))
            (#\$
             (make-chest row col depth (list (make-instance 'prop :collectible t :look (one-of 'sprite-gun-space 'sprite-goggles 'sprite-book 'sprite-table)))))
            (otherwise nil))))
    (when maybe-entity
      (set-position maybe-entity (list row col depth))
      maybe-entity)))

(defun %generate-level (depth)
  (let ((map (generate-map
              (make-instance
               'bsp-mapgen
               :rows 32 :cols 48
               :min-width (if (< depth 5) 4 12) :min-height (if (< depth 5) 3 10)
               ;; TODO idea was to use the following for higher levels to only spawn 1 enemy ("the boss")
               ;; (let ((ctr 0))
               ;;   (lambda (room)
               ;;     (when (zerop ctr)
               ;;         (progn
               ;;           (incf ctr)
               ;;           (let ((pos (random-position room (lambda (&rest _) t) :retries 100)))
               ;;             (list (cons pos #\s)))))))
               :room-entity-mapper
               (room-entity-mapper-by-level depth)
               :rooms-postprocess #'%rooms-postprocess-add-chest-and-stairs))))
    (map-to-level *level-store* (alexandria:copy-array map) depth #'%generate-level-add-tiles)
    (map-add-entities *entity-store* map depth #'%generate-level-to-entity)))

(defun %process-actor-event (event)
  (let* ((actor (gethash (id event) (entities *entity-store*)))
         (dist (vector-length-8 (rect-between actor *player*)))
         (action 'pass-turn))
    (when (< dist (fov actor))
      (let* ((prev
               (nth-value
                1 (dijkstra (current-level *level-store*)
                            (lambda (entity)
                              (cond
                                ((blockingp entity) 1000)
                                (t 1)))
                            :directions
                            (case (look actor)
                              (sprite-snake *directions4*)
                              ;; non-snake enemies use 8 directional movement
                              (otherwise *directions*))
                            :src (cons (posx *player*) (posy *player*))
                            :dest (cons (posx actor) (posy actor)))))
             (path (path-from-dijkstra-map prev (posx actor) (posy actor))))
        (setf action (simple-monster-ai path #'%simple-ai-failp #'%simple-ai-successp))))
    (unless (eq action 'pass-turn)
      (format *debug-stream* "PROCESS ACTOR ~a: action: ~a~%" (id actor) action))
    (typecase action
      (rect
       (move-entity* *entity-store* actor (rect-x1 action) (rect-y1 action))
       (schedule-event *event-handler* actor))
      (symbol
       (case action
         (pass-turn
          (handle-event *event-handler* (make-instance 'pass-turn-event :actor actor))
          (schedule-event *event-handler* actor))
         (invalid-path (error "expect valid path")))))))

(defun %waiting-for-player-input (&optional next)
  (let ((event (if next next (peek-event *event-handler*))))
    (and (= (id event) (id *player*))
         (null (input-stack *input-handler*)))))

(defun %scheduler-handle-action-result (scheduler actor action)
  (cond
    ((or (null action)
         (eq action 'do-nothing)) ; FIXME schedule actor at delay?
     (schedule-event scheduler actor :delay 0))
    ((eq action 'change-level)
     (%initialize-level (1+ (posz actor))))
    (t
     (schedule-event scheduler actor))))

(defun %process-actors ()
  (loop
    for next = (peek-event *event-handler*)
    ;; we don't want to extract the player event before an input is available
    until (or (null next) (%waiting-for-player-input next)) do
      (cond
        ((entity-removed-p *entity-store* (id *player*)) ; game over
         (format *debug-stream* "Player dead. Game over.~%")
         (format t "Player dead. Game over.~%")
         (setf (look (aref (current-level *level-store*) (posx *player*) (posy *player*)))
               'sprite-gravestone)
         (return-from %process-actors 'game-over))
        (t
         (let ((event (next-event *event-handler*)))
           (when event
             (format *debug-stream* "processing event id ~a time ~a~%" (id event) (scheduled-at event))
             (if (= (id event) (id *player*))
                 ;; handle player input
                 (if (input-stack *input-handler*)
                     (progn
                       (%scheduler-handle-action-result
                        *event-handler* *player*
                        (handle-input *entity-store* (car (input-stack *input-handler*))))
                       (setf (input-stack *input-handler*) nil))
                     (error "no input"))
                 (%process-actor-event event))))))))

(defun %schedule-actors (scheduler actors &key delay)
  (dolist (actor actors)
    (schedule-event scheduler actor :delay delay)))

(defun set-neighbor-position (entity other-entity)
  (set-position other-entity (car (walkable-neighbor-positions entity))))

(defun %player-starting-position ()
  (random-position *level-store* (lambda (entity) (not (blockingp entity)))))

(defun %actors-add-items-from-loot-table (depth)
  (loop for actor-cons in (get-actors-in-level *entity-store* depth)
        for actor = (car actor-cons) do
          (let ((loot-table (trait actor 'loot-table)))
            (when loot-table
              (loop for item-symbol in (funcall loot-table) do
                (push-trait-value actor 'inventory
                                  (funcall (gethash item-symbol *items*))))))))

(defun %initialize-level (depth)
  (setf (depth *level-store*) depth
        (posz *player*) depth)
  (%generate-level depth)
  (set-position *player* (%player-starting-position))
  ;; schedule player with 0 delay so enemies don't act before the 1st turn
  (schedule-event *event-handler* *player* :delay 0)
  (%schedule-actors
   *event-handler*
   (remove-if
    (lambda (actor)
      ;; don't schedule player twice
      (or (= (id actor) (id *player*))
          (not (= (posz actor) depth))))
    (mapcar (lambda (id)
              (get-entity *entity-store* id))
            (actors *entity-store*))))
  (%actors-add-items-from-loot-table depth))

(defun wizkeep ()
  (unwind-protect
       (flet ((accept-input (store input)
                (declare (ignore store))
                (when input
                  (push input (input-stack *input-handler*)))))
         ;; add player before generating level so player gets id 0
         (initialize-player)
         (%initialize-level 0)
         (ui-toplevel :entity-store *entity-store* :level-store *level-store*
                      :handle-inputs #'accept-input
                      :run-turns #'%process-actors)))
  (progn
    (reset *entity-store*)
    (reset *level-store*)
    (reset *input-handler*)
    (reset *event-handler*)))
