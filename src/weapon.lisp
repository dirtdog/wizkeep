;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass weapon ()
  ((dice :accessor dice
         :initarg :dice
         :initform (d 2))
   (weight :accessor weight
           :initarg :weight
           :initform 1)
   (verb-string :accessor verb-string
                :initarg :verb-string
                :initform "hits ~a"
                :documentation "String passed to FORMAT. Must contain 1 ~A statement for the victim of an attack.")))

(defmacro defweapon (class-name super-class dice &optional verb-string)
  (let ((verb-string-list (when verb-string `((verb-string :initform ,verb-string))))
        (super-class-list
          (cond
            ((null super-class) `(weapon))
            ((listp super-class) super-class)
            (t `(,super-class)))))
      `(defclass ,class-name ,super-class-list
         ((dice :initform ,dice)
          ,@verb-string-list))))

(defweapon cons-knuckle () (d 3) "hit ~a with cons knuckle")
(defweapon weapon-hash-table () (d 3 3) "thwack ~a with your hashing table")
(defweapon weak-lambda-bite () (d 3) "with a single expression, bites ~a")
(defweapon crab-claw () (d 1 1 1) "pincers ~a")
(defweapon spring-boot () (d 4 1 1) "kicks ~a with a spring-launched boot")
(defweapon callback-weapon () (d 2) "has an Uncaught Typo, triggering ~a")

(defgeneric weapon-verb (weapon victim)
  (:method ((weapon weapon) victim)
    (format nil (verb-string weapon) (look victim))))

(defparameter *weapon-unarmed* (make-instance 'weapon))
(defparameter *weapon-snake* (make-instance 'weak-lambda-bite))
(defparameter *weapon-cons* (make-instance 'cons-knuckle))
(defparameter *weapon-crab-claw* (make-instance 'crab-claw))
(defparameter *weapon-spring-boot* (make-instance 'spring-boot))
(defparameter *weapon-callback* (make-instance 'callback-weapon))
