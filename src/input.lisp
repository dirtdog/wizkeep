;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass input-handler ()
  ((input-stack :accessor input-stack
                :initform ())))

(defmethod reset ((handler input-handler))
  (setf (input-stack handler) nil))

(defun handle-input (entity-store input)
  (cond
    ((find input *directions*)
     (move entity-store (player entity-store) input))
    ((eq input 'dwim)
     (if (player-standing-on-staircase-p entity-store)
         'change-level
         'do-nothing))
    (t
     (warn "unknown input ~a" input)
     'do-nothing)))
