;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defclass entity-store ()
  ((entities :accessor entities
             :initarg :entities
             :initform (make-hash-table))
   ;; TODO need a way to map position -> id
   (actors :accessor actors
           :initform ()
           :documentation "Set of actor IDs. Actors are entities stored in slot ENTITIES.")
   (props :accessor props
          :initform ()
          :documentation "Set of prop IDs. Props are entities stored in slot ENTITIES.")
   (event-handler :reader event-handler
                  :initarg :event-handler)
   (next-id :accessor next-id
            :initform 0)))

(defmethod get-entity ((store entity-store) id)
  (restart-case
      (multiple-value-bind (entity exists)
          (gethash id (entities store))
        (if exists
            entity
            (error "no entity with id ~a" id)))
    (use-value (new-id)
      :interactive
      (lambda ()
        (prompt-new-value "Enter another entity id: "))
      (get-entity store new-id))))

(defgeneric entity-exists (store entity)
  (:method ((store entity-store) id)
    (nth-value 1 (gethash id (entities store))))
  (:method ((store entity-store) (entity entity))
    (nth-value 1 (gethash (id entity) (entities store)))))

(defmethod entity-removed-p ((store entity-store) id)
  (not (nth-value 1 (gethash id (entities store)))))

(defgeneric reset (place)
  (:method ((store entity-store))
    (setf (entities store) (make-hash-table)
          (actors store) nil
          (props store) nil
          (next-id store) 0)))

(defmethod player ((store entity-store))
  (gethash 0 (entities store)))

(defmethod actors-maybe-add-entity ((store entity-store) (entity entity))
  "If entity is an actor, adds it to the actors set and returns non-nil."
  (when (actorp entity)
    (restart-case
        (progn
          (when (eq (id entity) 'unset)
            (error "Actor id is UNSET"))
          (setf (actors store)
                (adjoin (id entity) (actors store))))
      (use-next-id ()
        :report (lambda (stream)
                  (format stream "Use next id (~a)" (next-id store)))
        (let ((id (next-id store)))
          (incf (next-id store))
          (setf (id entity) id))))))

(defmethod actors-remove-entity ((store entity-store) id)
  (setf (actors store)
        (remove id (actors store) :count 1)))

(defmethod add-trait ((store entity-store) (entity entity) trait &optional (value t))
  (unless (traits entity)
    (setf (traits entity)
          (make-hash-table)))
  (setf (gethash trait (traits entity))
        value)
  (actors-maybe-add-entity store entity)
  entity)

(defmethod remove-trait ((store entity-store) (entity entity) trait)
  "Returns (values TRAIT-VALUE WAS-REMOVED)."
  (let ((was-actor (actorp entity)))
    (multiple-value-bind (trait-val has-trait)
        (gethash trait (traits entity))
      (if has-trait
          (progn
            (remhash trait (traits entity))
            (when (and was-actor (not (actorp entity)))
              (actors-remove-entity store (id entity)))
            (values trait-val t))
          (values nil nil)))))

(defmethod add-to-props ((store entity-store) prop)
  (setf (props store)
        (adjoin (id prop) (props store))))

(defgeneric props-maybe-add-entity (store entity)
  (:documentation "If entity is an actor, adds it to the actors set and returns non-nil.")
  (:method ((store entity-store) entity)
    nil)
  (:method ((store entity-store) (prop prop))
    (restart-case
        (progn
          (when (eq (id prop) 'unset)
            (error "Prop id is UNSET"))
          (add-to-props store prop))
      (use-next-id ()
        :report (lambda (stream)
                  (format stream "Use next id (~a)" (next-id store)))
        (let ((id (next-id store)))
          (incf (next-id store))
          (setf (id prop) id))))))

(defgeneric props-remove-entity (store prop)
  (:method ((store entity-store) id)
    (setf (props store)
          (remove id (props store) :count 1)))
  (:method ((store entity-store) (prop prop))
    (setf (props store)
          (remove (id prop) (props store) :count 1)))
  (:method ((store entity-store) (props list))
    (setf (props store)
          (set-difference (props store)
                          (mapcar (lambda (prop) (id prop))
                                  props)))))

(defmethod add-entity ((store entity-store) (entity entity))
  (let ((id (next-id store)))
    (incf (next-id store))
    (setf (id entity) id
          (gethash id (entities store)) entity)
    (actors-maybe-add-entity store entity)
    (props-maybe-add-entity store entity)
    entity))

(defmethod add-entity-props ((store entity-store) props position)
  (dolist (prop props)
    (set-position prop position)
    (if (not (entity-exists store prop))
        (add-entity store prop)
        (warn "prop ~a exists" (id prop)))))

(defgeneric remove-entity (store entity)
  (:method ((store entity-store) (id integer))
    (let* ((entity (gethash id (entities store)))
           (was-actor (actorp entity)))
      (remhash id (entities store))
      (when was-actor
        (actors-remove-entity store id))
      (when (prop-p entity) ; remove prop if exists
        (setf (props store)
              (remove id (props store) :count 1)))
      entity))
  (:method ((store entity-store) (entity entity))
    (remove-entity store (id entity))))

(defmethod get-actors-in-level ((store entity-store) level)
  "Returns alist of actors at depth LEVEL with key being the actor and values
 being the actor position as a list."
  (loop for id in (actors store)
        for actor = (gethash id (entities store))
        when (= (posz actor) level)
          collect (cons actor (get-position actor))))

(defmethod get-actor-at-position ((store entity-store) position)
  (destructuring-bind (x y z) position
    (loop for id in (actors store)
          for actor = (gethash id (entities store))
          do
             (when (and (= (posx actor) x)
                        (= (posy actor) y)
                        (= (posz actor) z)
                        (collision actor))
               (return actor)))))

(defmethod get-props-in-level ((store entity-store) level)
  "Returns alist of props at depth LEVEL with key being the prop and values
 being the prop position as a list."
  (loop for id in (props store)
        for prop = (gethash id (entities store))
        when (= (posz prop) level)
          collect (cons prop (get-position prop))))

(defmethod get-props-at-position ((store entity-store) position)
  "Returns list of props at POSITION."
  (destructuring-bind (x y z) position
    (loop for id in (props store)
          for prop = (gethash id (entities store))
          when (and (= (posx prop) x)
                    (= (posy prop) y)
                    (= (posz prop) z))
            collect prop)))

(defmethod player-standing-on-staircase-p ((store entity-store))
  (remove-if (lambda (prop) (not (eq (look prop) 'sprite-stair-up)))
             (get-props-at-position store (get-position (player store)))))

(defun %prop-sort-predicate (prop-a prop-b)
  (typecase prop-a
    (container-prop t)
    (otherwise
     (if (eq prop-a 'sprite-key-silver)
         ;; key first
         t
         (or (and (collision prop-a) (not (collision prop-b)))
             (< (id prop-a) (id prop-b)))))))

;; TODO defgeneric, instead of x y, take position argument (cons, list or struct) (?)
(defgeneric move-entity* (store entity x y &key depth)
  (:method ((store entity-store) (entity entity) x y &key depth)
    (restart-case
        (if (or (< x 0) (>= x 32) ;; FIXME MAGIC NUMBER
                (< y 0) (>= y 48)) ;; --..--
            (error "invalid index (~a ~a) - cannot move ~a" x y entity)
            (let* ((depth (if depth depth (posz entity)))
                   (actor-at (get-actor-at-position store (list x y depth)))
                   (props-at (unless actor-at (get-props-at-position store (list x y depth))))
                   (collision-tile-at
                     (unless (or actor-at props-at)
                       (loop for other being the hash-values in (entities store)
                             do (when (and (= (posx other) x)
                                           (= (posy other) y)
                                           (= (posz other) depth)
                                           (collision other))
                                  (return other))))))
              (cond
                (actor-at
                 (format *debug-stream* "attacking actor ~a at (~a ~a)~%"
                         (look actor-at) x y)
                 (attack store entity actor-at))
                (props-at
                 (car
                  ;; process each prop (e.g. monster may have dropped multiple
                  ;; items from its inventory)
                  (loop for prop in (sort (copy-list props-at) #'%prop-sort-predicate)
                        for ctr from 0
                        collect
                        (multiple-value-bind (outcome entity-moved)
                            (move-onto entity prop)
                          (cond
                            ((null outcome)
                             ;; action blocked for whatever reason
                             'do-nothing)
                            ((eq outcome 'opened)
                             (handle-event
                              (event-handler store)
                              (make-instance
                               'prop-collect-event :actor entity
                                                   :props prop
                                                   :outcome 'opened))
                             (add-entity-props store (content prop) (get-position prop)))
                            ((or (listp outcome) (prop-p outcome))
                             ;; collection occurred
                             (props-remove-entity store outcome)
                             (handle-event
                              (event-handler store)
                              (make-instance 'prop-collect-event :actor entity
                                                                 :props outcome
                                                                 :outcome t)))
                            (entity-moved
                             ;; actor moved onto prop tile
                             ;; (check we don't make many movement events for moving to the same position)
                             (when (zerop ctr)
                               (handle-event (event-handler store)
                                             (make-instance 'move-event :actor entity)))))))))
                (collision-tile-at
                 'do-nothing)
                (t
                 (handle-event (event-handler store)
                               (make-instance 'move-event :actor entity))
                 (set-position entity (list x y depth))))))
      (do-nothing () nil)))
  (:method :before (store (actor entity) x y &key depth)
    (before-actor-turn actor)))

(defmethod move ((store entity-store) (entity entity) direction)
  (flet ((move-with-offset (dx dy)
           (let ((posx (posx entity))
                 (posy (posy entity)))
             (move-entity* store entity (+ dx posx) (+ dy posy)))))
    (apply #'move-with-offset (direction-list direction))))

(defgeneric attack (store actor victim)
  (:method ((store entity-store) (actor entity) (victim entity))
    (multiple-value-bind (weapon has-weapon)
        (trait actor 'current-weapon)
      (let ((dmg (roll-dice
                  (dice (if has-weapon
                            weapon
                            *weapon-unarmed*)))))
        (handle-event
         (event-handler store)
         (make-instance 'attack-event :actor actor :victim victim :weapon weapon :damage dmg))
        (when (take-attack victim dmg) ; dead
          ;; no need to drop player's inventory
          (let ((drops (unless (eq (player store) victim) (trait victim 'inventory))))
            (when drops
              (let ((pos (get-position victim)))
                (add-entity-props store drops pos)))
            (handle-event (event-handler store)
                          (make-instance 'actor-died-event :actor victim :drops drops)))
          (remove-entity store victim))
        t)))
  (:method :after (store actor victim)
    (after-actor-turn actor 'attack)))

(defmethod visible-actors ((store entity-store) &key (depth 0))
  (let* ((player-depth (posz (player store)))
         (actors (get-actors-in-level store player-depth)))
    (remove-if
     (lambda (entity)
       (and entity
            (not (eq (id entity) (id *player*)))
            (> (vector-length-8 (rect-between entity (player store)))
               (fov (player store)))))
     (append (mapcar #'car actors)
             (mapcar (lambda (cons)
                       (destructuring-bind (prop . position) cons
                         ;; only display props when no actor is on that tile
                         (unless (rassoc position actors :test #'equal)
                           prop)))
                     (get-props-in-level store player-depth))))))
