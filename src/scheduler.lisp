;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defvar *actor-event-stream* t)

(defclass scheduled-event ()
  ((id :accessor id
       :initarg :id)
   (scheduled-at :accessor scheduled-at
                 :initarg :scheduled-at)))

(defmethod scheduled-before ((event-a scheduled-event) (event-b scheduled-event))
  (< (scheduled-at event-a) (scheduled-at event-b)))

(defclass scheduler ()
  ((schedule :reader schedule
             :initform (serapeum:make-heap :test #'scheduled-before))
   (tick :accessor tick
         :initform 0)
   (cancelled :accessor cancelled
              :initform (make-hash-table))))

(defmethod print-object ((event scheduled-event) stream)
  (print-unreadable-object (event stream :type t)
    (format stream "id ~a, scheduled ~a" (id event) (scheduled-at event))))

(defgeneric schedule-event (scheduler event &key delay)
  (:method ((scheduler scheduler) (actor entity) &key delay)
    (let* ((sched-time
             (+ (tick scheduler)
                (if (null delay)
                    (trait actor 'speed 1)
                    delay)))
           (sched-event (make-instance 'scheduled-event
                                       :id (id actor)
                                       :scheduled-at sched-time)))
      (serapeum:heap-insert
       (schedule scheduler)
       sched-event)
      sched-event))
  (:method ((scheduler scheduler) (entity gc-entity) &key delay)
    (if (and delay (= delay 0))
        ;; if delay is explicitly set to 0, then the turn is a "no-op",
        ;; so don't mess entity with state
        (call-next-method scheduler entity)
        (let ((new-delay (+ (trigger-gc entity) (or delay 0))))
          (call-next-method scheduler entity :delay (unless (zerop new-delay) new-delay))))))

(defgeneric peek-event (scheduler)
  (:method ((scheduler scheduler))
    (serapeum:heap-maximum (schedule scheduler))))

(defmethod entity-removed-p ((scheduler scheduler) id)
  (nth-value 1 (gethash id (cancelled scheduler))))

(defgeneric next-event (scheduler)
  (:method ((scheduler scheduler))
    (let ((event (serapeum:heap-extract-maximum (schedule scheduler))))
      (unless (entity-removed-p scheduler (id event))
        (setf (tick scheduler) (scheduled-at event))
        event))))

(defmethod has-events ((scheduler scheduler))
  (not (null (serapeum:heap-maximum (schedule scheduler)))))

(defmethod reset ((scheduler scheduler))
  (setf (tick scheduler) 0
        (cancelled scheduler) (make-hash-table))
  (serapeum:heap-extract-all (schedule scheduler)))

(defmethod increment-tick ((scheduler scheduler) actor)
  (incf (tick scheduler) (trait actor 'speed 1)))

(defun %print-actor-stream (string)
  (unless (zerop (length string))
    (format *event-stream* "~a " string)))

(defgeneric handle-event (handler event)
  (:method ((scheduler scheduler) event)
    (%print-actor-stream (event-string event)))
  (:method ((scheduler scheduler) (event actor-died-event))
    (%print-actor-stream (event-string event))
    (setf (gethash (id (actor event)) (cancelled scheduler)) t)
    nil))
