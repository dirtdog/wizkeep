;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(in-package :wizkeep)

(defun dijkstra (vertices weight-of &key (src '(0 . 0)) (directions *directions4*) dest previous-as-ref)
  "Given an MxN matrix VERTICES, returns (values DISTANCES PREVIOUS), each of
which is also an MxN matrix. WEIGHT-OF is a function for vertex => number. Each
(row col)-pair in DISTANCES contains the distance of the path from SRC to (row
col). PREVIOUS contains (row col)-pairs to the previous vertex in the path. If
PREVIOUS-AS-REF is true, each entry in PREVIOUS will instead contain the
reference to the previous vertex from VERTICES."
  (destructuring-bind (rows cols)
      (array-dimensions vertices)
    (let* ((distances (make-array (array-dimensions vertices)
                                  :initial-element most-positive-fixnum))
           (previous (make-array (array-dimensions vertices)
                                 :initial-element nil))
           (queue
             (serapeum:make-heap
              :test (lambda (u v)
                      (< (aref distances (car u) (cdr u))
                         (aref distances (car v) (cdr v))))
              ;; expect slightly more than (max rows cols) in the queue at once
              :size (+ (max rows cols) 16)))
           (in-queue (make-array (array-dimensions vertices) :adjustable nil
                                                             :element-type 'bit
                                                             :initial-element 0)))
      (labels ((enqueue (item)
                 (setf (aref in-queue (car item) (cdr item)) 1)
                 (serapeum:heap-insert queue item))
               (dequeue ()
                 (let ((min (serapeum:heap-extract-maximum queue)))
                   (setf (aref in-queue (car min) (cdr min)) 0)
                   min))
               (neighbor-direction (pos direction)
                 (let ((newpos (shift-position-pair pos direction)))
                   (unless (or (< (car newpos) 0)
                               (>= (car newpos) rows)
                               (< (cdr newpos) 0)
                               (>= (cdr newpos) cols))
                     newpos)))
               (neighbors (vertex)
                 (remove-if #'null
                            (mapcar (lambda (direction)
                                      (neighbor-direction vertex direction))
                                    directions))))
        (setf (aref distances (car src) (cdr src)) 0)
        (enqueue src)
        (block find-paths
          (loop while (serapeum:heap-maximum queue)
                for u = (dequeue)
                for neighbors = (neighbors u) do
                  (destructuring-bind (i . j) u
                    (dolist (v neighbors)
                      (destructuring-bind (other-i . other-j) v
                        (let ((alt (+ (aref distances i j)
                                      (funcall weight-of (aref vertices other-i other-j)))))
                          (when (< alt (aref distances other-i other-j))
                            (when (zerop (aref in-queue other-i other-j))
                              (enqueue v))
                            (setf (aref distances other-i other-j) alt
                                  (aref previous other-i other-j) u)
                            (when (and dest
                                       (= other-i (car dest))
                                       (= other-j (cdr dest)))
                              (return-from find-paths)))))))))
        (when previous-as-ref
          (with-matrix-indices (row col) vertices
            (setf (aref previous row col) (aref vertices row col))))
        (values distances previous)))))
