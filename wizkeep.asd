;; Copyright (C) 2024 Elvin Stevens (dirtdog@posteo.net)
;;
;; This file is part of wizards-keep.
;;
;; wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
;;
;; wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.

(require "asdf")

(defsystem "wizkeep"
  :author "Elvin Stevens"
  :license "GPLv3+"
  :description "Roguelike for 2024 Lisp Game Jam."
  :version "0.0.1"
  :depends-on ("alexandria"
               "serapeum"
               "sdl2"
               "sdl2-image")
  :components
  ((:module "src"
    :serial t
    :components
    ((:file "package")
     (:file "util")
     (:file "rect")
     (:file "weapon")
     (:file "entity")
     (:file "prop")
     (:file "items")
     (:file "event")
     (:file "player")
     (:file "scheduler")
     (:file "entity-store")
     (:file "enemies")
     (:file "level")
     (:file "input")
     (:file "sdl")
     (:file "pathfinding")
     (:file "mapgen")
     (:file "wizkeep")))))
