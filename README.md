# Wizard's Keep

Roguelike game.
Entry for the *Spring Lisp Game Jam 2024*.

## How to Run It

### Prerequisites

- Install sdl2 and sdl2-image dynamic libraries
- Install a Common Lisp implementation (SBCL 2.4.0 was used during development)
- Common Lisp dependencies are included in the `deps/` directory, and will be loaded from here by default.
  *To use system installed dependencies instead, remove the following line from* `init.lisp`:
```
(load "deps/bundle.lisp")
```

### Running

Open the REPL (e.g. SBCL) in the top level directory and run the following commands (omitting `CL-USER>`):

```lisp
CL-USER> (load "init.lisp")
```

The first time this will take a while since it will compile the game and its dependencies.

### Controls

- Movement:
  - vi keys (h j k l) to move left, down, up, right respectively
  - further (y u b n) moves upleft, upright, downleft, downright respectively
  - Moving into enemies attacks them
  - Moving into chests while holding a key opens them
- space: while standing on a staircase press space to go to the next level
- q: close the game (can be restarted from the REPL)

## Known Limitations

- FoV: You can view enemies that are close enough through walls
- Enemy pathfinding: Sometimes an enemy may attack another if its path is blocked
- Loot: Item pickups have no effects (other than keys unlocking the ability to open chests)
- Log output: For some reason the log is not as wide as it should be

## Resources: Map Generation

- [Basic BSP Dungeon generation (roguebasin)](https://roguebasin.com/index.php/Basic_BSP_Dungeon_generation)
- [Herbert Wolverson - Procedural Map Generation Techniques, Roguelike Celebration](https://www.youtube.com/watch?v=TlLIOgWYVpI)

See more comments in the README.md in the devlog directory.

## Copyright Disclaimer

This file is part of wizards-keep.

wizards-keep is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

wizards-keep is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wizards-keep. If not, see <https://www.gnu.org/licenses/>.
